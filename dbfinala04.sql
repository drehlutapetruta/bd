DROP TABLE IF EXISTS InfoStudenti; 
DROP TABLE IF EXISTS InfoCazari;
DROP TABLE IF EXISTS InfoCamere;
DROP TABLE IF EXISTS Studenti;
DROP TABLE IF EXISTS Facultate;
DROP TABLE IF EXISTS Camere;
DROP TABLE IF EXISTS Camine;
DROP TABLE IF EXISTS Campusuri;
DROP TABLE IF EXISTS InfoChirii;
DROP TABLE IF EXISTS Admini;
DROP TABLE IF EXISTS Vizitatori;
DROP TABLE IF EXISTS RestantieriPlata;
DROP TABLE IF EXISTS Evacuati;
 
CREATE TABLE Campusuri (
	id_Campus SERIAL NOT NULL 
		CONSTRAINT pk_id_Campus PRIMARY KEY,
	NumeCampus VARCHAR(24) NOT NULL,
	Cantina BOOLEAN,
	NrCamere NUMERIC(4)
	);

CREATE TABLE Camine (
	id_Camin SERIAL NOT NULL 
		CONSTRAINT pk_id_Camin PRIMARY KEY,
	NumeCamin VARCHAR(24) NOT NULL,
	Adresa VARCHAR(128),
	NrEtaje smallint,
	NrCamere NUMERIC (4),
	NumeAdm VARCHAR (64),
	id_Campus smallint
		CONSTRAINT fk_camine REFERENCES Campusuri(id_Campus)
	);

CREATE TABLE Camere (
	id_Camera NUMERIC (4) NOT NULL
		CONSTRAINT pk_id_Camere PRIMARY KEY,
	NrPersCam smallint,
	NrLocOcup smallint,
	NrLocLibere smallint,
	Baie BOOLEAN,
	TipCamera CHAR(1) DEFAULT 'M' NOT NULL CONSTRAINT ck_tipcamera CHECK (TipCamera IN ('F', 'M')),
	id_Camin smallint
		CONSTRAINT fk_Camere REFERENCES Camine(id_Camin)
	);
	
CREATE TABLE Facultate (
	id_Facultate SERIAL NOT NULL
		CONSTRAINT pk_id_Facultate PRIMARY KEY,
	NumeFac VARCHAR(48) NOT NULL,
	NumeDecan VARCHAR(64)
	);

CREATE TABLE Studenti (
	id_Stud SERIAL NOT NULL
		CONSTRAINT pk_id_Studenti PRIMARY KEY,
	NumeStud VARCHAR(24) NOT NULL,
	PrenumeStud VARCHAR(24) NOT NULL,
	CNP NUMERIC (13) NOT NULL,
	Sex CHAR(1) DEFAULT 'M' NOT NULL CONSTRAINT ck_sex CHECK (Sex IN ('F', 'M')),
	Adresa VARCHAR(128),
	/*BenefCamin BOOLEAN default true,*/
	id_Camera smallint
		/*CONSTRAINT fk_Studenti REFERENCES Camere(id_Camera)*/,
	id_Facultate smallint
		CONSTRAINT fk_Studenti1 REFERENCES Facultate(id_Facultate)
	);


CREATE TABLE InfoCamere(
	id_Camin smallint NOT NULL,
	id_Camera smallint NOT NULL,
	Etaj smallint NOT NULL,
	NrCamera NUMERIC (4), 
	PRIMARY KEY (id_Camin, id_Camera, Etaj)
	);
	
CREATE TABLE InfoCazari (
	id_Camin smallint NOT NULL,
	id_Stud smallint NOT NULL,
	id_Camera NUMERIC (4) NOT NULL, 
	Chirie NUMERIC (4),
	DataCazare DATE NOT NULL,
	DataDecazare DATE,
	PRIMARY KEY (id_Camin, id_Stud, id_Camera, DataCazare)
	);
	
CREATE TABLE InfoStudenti (
	id_Camin smallint,
	id_Stud smallint NOT NULL,
	id_Facultate smallint NOT NULL,
	Medie NUMERIC(4),
	PRIMARY KEY (id_Stud, id_Facultate)
	);

CREATE TABLE Admini(
	id_admin SERIAL NOT NULL,
	Nume_admin varchar(30),
	Email varchar(30) NOT NULL UNIQUE,
	Username varchar(30) NOT NULL UNIQUE,
	Parola CHAR(255) NOT NULL,
	Grad smallint NOT NULL,
	PRIMARY KEY (id_admin, Username)
	);
CREATE TABLE Vizitatori(
	id_vizitator SERIAL NOT NULL
		CONSTRAINT pk_idvizitator PRIMARY KEY,
	Nume_Vizitator varchar(30) NOT NULL,
	CNP_Vizitator numeric(13) NOT NULL,
	NumeCamin varchar(24) NOT NULL,
	NrCamera numeric(3) NOT  NULL,
	DataVizitei DATE,
	OraSosire time,
	OraPlecare time
	);
CREATE TABLE InfoChirii(
	id_Stud smallint NOT NULL,
	id_Camin smallint NOT NULL,
	NumeStud varchar(24) NOT NULL,
	PrenumeStud VARCHAR(24) NOT NULL,
	Chirie numeric(4),
	SoldCurent numeric(4),
	--Observatii varchar (24),
	PRIMARY KEY (id_Stud, id_Camin)
	);
	
CREATE TABLE RestantieriPlata(
	id_Stud smallint NOT NULL
		CONSTRAINT pk_id_Stud_restant PRIMARY KEY,
	NumeStud varchar(24) NOT NULL,
	PrenumeStud VARCHAR(24) NOT NULL,
	Avertismente numeric(2),
	NrZileRestante numeric(3)
	);

CREATE TABLE Evacuati (
    id_Stud smallint NOT NULL
		CONSTRAINT pk_id_Stud_Evacuat PRIMARY KEY,
	NumeStud VARCHAR(24) NOT NULL,
	PrenumeStud VARCHAR(24) NOT NULL
	);

/* Campusuri */
INSERT INTO Campusuri (NumeCampus, Cantina, NrCamere) 
	VALUES ('Targusor-Copou', TRUE, 1750);
INSERT INTO Campusuri (NumeCampus, Cantina, NrCamere) 
	VALUES ('Titu Maiorescu', TRUE, 2286);
INSERT INTO Campusuri (NumeCampus, Cantina, NrCamere) 
	VALUES ('Codrescu', FALSE, 1800);
INSERT INTO Campusuri (NumeCampus, Cantina, NrCamere) 
	VALUES ('Gaudeamus', TRUE, 400);
INSERT INTO Campusuri (NumeCampus, Cantina, NrCamere) 
	VALUES ('Akademos', TRUE, 500);

/* Camine */
INSERT INTO Camine (NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES ('C1', 'Str. Stoicescu nr.1-4' , 3 , 412, 'Georgeta Ailenii', 1);
INSERT INTO Camine (NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES ('C2', 'Str. Stoicescu nr.1-4' , 3, 449, 'Georgeta Ailenii', 1);
INSERT INTO Camine (NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES ('C3', 'Str. Stoicescu nr.1-4' , 3, 417, 'Elena Ciobanu', 1);
INSERT INTO Camine (NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES ('C4', 'Str. Stoicescu nr.1-4' , 3, 436, 'Elena Ciobanu', 1);
INSERT INTO Camine (NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES ('C5', 'Str. Titu Maiorescu nr.7-9' , 4, 1356, 'Maria-Mihaela Tuiu', 2);
INSERT INTO Camine (NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES ('C6', 'Str. Titu Maiorescu nr.7-9' , 4, 1356, 'Maria-Mihaela Tuiu', 2);
INSERT INTO Camine (NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES ('C7', 'Str. Titu Maiorescu nr.7-9' , 4, 1357, 'Ec. Doina-Elena Epure', 2);
INSERT INTO Camine (NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES ('C8', 'Str. Titu Maiorescu nr.7-9' , 4, 1357, 'Ec. Doina-Elena Epure', 2);
INSERT INTO Camine (NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES ('C10', 'Str.Codrescu, nr. 7 ' , 5, 453, 'Ec. Valeria Ciobanu', 3);
INSERT INTO Camine (NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES ('C11', 'Str.Codrescu, nr. 7 ' , 5, 316, 'Ec. Mihaela-Lenuta Boureanu', 3);
INSERT INTO Camine (NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES ('C12', 'Str.Codrescu, nr. 7 ' , 5, 557, 'Ec. Mihaela-Lenuta Boureanu', 3);
INSERT INTO Camine (NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES ('C13', 'Str.Codrescu, nr. 7 ' , 5, 463, 'Dana-Mihaela Raba', 3);
INSERT INTO Camine (NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES ('Gaudeamus', 'Str. Codrescu Nr.1' , 2, 400, 'Ec. Teodora Tanasa', 4);
INSERT INTO Camine (NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES ('Akademos', 'Str. Pacurari, nr. 6' , 2, 500, 'Ec. Teodora Tanasa', 5);

/* Camere */
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (1, 4, 4, 0, TRUE, 'M', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (2, 4, 3, 1, FALSE, 'M', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (3, 4, 2, 2, TRUE, 'M', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (4, 4, 1, 3, TRUE, 'M', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (5, 3, 3, 0, TRUE, 'F', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (6, 3, 3, 0, TRUE, 'F', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (7, 3, 2, 1, TRUE, 'F', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (8, 3, 3, 0, TRUE, 'F', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (9, 3, 2, 1, TRUE, 'F', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (100, 4, 4, 0, FALSE, 'M', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (101, 4, 4, 0, FALSE, 'M', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (102, 4, 4, 0, FALSE, 'M', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (103, 4, 4, 0, FALSE, 'M', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (104, 4, 4, 0, FALSE, 'M', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (105, 5, 4, 1, FALSE, 'F', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (106, 5, 4, 1, FALSE, 'F', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (107, 4, 4, 0, FALSE, 'F', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (108, 4, 4, 0, FALSE, 'F', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (109, 5, 5, 0, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (200, 5, 5, 0, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (201, 4, 3, 1, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (202, 4, 4, 0, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (203, 4, 2, 2, TRUE, 'F', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (204, 2, 2, 0, TRUE, 'F', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (205, 2, 2, 0, TRUE, 'F', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (206, 3, 3, 0, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (207, 3, 3, 0, FALSE, 'F', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (208, 3, 2, 1, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (209, 3, 2, 1, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (300, 4, 4, 0, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (301, 3, 3, 0, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (302, 5, 4, 1, FALSE, 'F', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (303, 2, 2, 0, TRUE, 'M', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (304, 4, 2, 1, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (305, 5, 5, 0, FALSE, 'F', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (306, 3, 2, 1, TRUE, 'M', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (307, 4, 2, 2, TRUE, 'F', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (308, 4, 4, 0, TRUE, 'M', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (309, 4, 4, 0, TRUE, 'M', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (400, 5, 5, 0, FALSE, 'M', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (401, 4, 3, 1, TRUE, 'M', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (402, 3, 3, 0, TRUE, 'F', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (403, 4, 3, 1, TRUE, 'F', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (404, 3, 3, 0, TRUE, 'F', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (405, 3, 3, 0, TRUE, 'F', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (406, 4, 3, 1, TRUE, 'M', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (407, 5, 4, 1, TRUE, 'M', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (408, 4, 4, 0, TRUE, 'F', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (409, 5, 5, 0, FALSE, 'M', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (500, 5, 5, 0, FALSE, 'M', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (501, 3, 3, 0, TRUE, 'F', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (502, 3, 3, 0, TRUE, 'F', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (503, 3, 3, 0, TRUE, 'F', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (504, 3, 3, 0, TRUE, 'F', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (505, 2, 2, 0, TRUE, 'M', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (506, 2, 2, 0, TRUE, 'M', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (507, 2, 2, 0, TRUE, 'M', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (508, 3, 2, 1, TRUE, 'M', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (509, 3, 3, 0, TRUE, 'M', 5);

/* Facultati */
INSERT INTO Facultate (NumeFac, NumeDecan)
	VALUES ('Biologie', 'Glotariu Cristi' );
INSERT INTO Facultate (NumeFac, NumeDecan)
	VALUES ('Chimie', 'Bladul Cornel' );
INSERT INTO Facultate (NumeFac, NumeDecan)
	VALUES ('Drept', 'Gicu Gheorghe' );
INSERT INTO Facultate (NumeFac, NumeDecan)
	VALUES ('Economie si Administrarea Afacerilor', 'Dinu Airiniei' );
INSERT INTO Facultate (NumeFac, NumeDecan)
	VALUES ('Educatie Fizica si Sport', 'Klaus Koro' );
INSERT INTO Facultate (NumeFac, NumeDecan)
	VALUES ('Filosofie si Stiinte Social-Politice', 'Buhai Groapa' );
INSERT INTO Facultate (NumeFac, NumeDecan)
	VALUES ('Fizica', 'Grigorescu Clerul' );
INSERT INTO Facultate (NumeFac, NumeDecan)
	VALUES ('Geografie si Geologie', 'Pasarescu Cristi' );
INSERT INTO Facultate (NumeFac, NumeDecan)
	VALUES ('Informatica', 'Bidon Ionut' );
INSERT INTO Facultate (NumeFac, NumeDecan)
	VALUES ('Istorie', 'Teava Andrei' );
INSERT INTO Facultate (NumeFac, NumeDecan)
	VALUES ('Litere', 'Servetel Toma' );
INSERT INTO Facultate (NumeFac, NumeDecan)
	VALUES ('Matematica', 'Mihail Mihailescu' );
INSERT INTO Facultate (NumeFac, NumeDecan)
	VALUES ('Psihologie si Stiint ale Educatiei', 'Drob Petru' );
INSERT INTO Facultate (NumeFac, NumeDecan)
	VALUES ('Teologie', 'Trompeta Grigoras' );


/* Studenti */
INSERT INTO Studenti (NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES ('Stroe', 'Mihaela', 2601228390834, 'F', 'Bd. Cantemir, 32, Bl.G4, Sc.C, Ap.4', 205, 3) ;
INSERT INTO Studenti (NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES ('Buzatu', 'Corneliu', 1650512370514, 'M', 'Str. Desprimaveririi, 112', 109, 1) ;
INSERT INTO Studenti (NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES ('Spineanu', 'Marius', 5010101380625, 'M', 'Bd. Stefan cel Mare, 4, Bl.I1, Sc.A, Ap.24', 8, 6) ;
INSERT INTO Studenti (NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES ('Bagdasar', 'Adela', 2601002250611, 'F', 'Str. Primaverii, 17', 304, 6) ;
INSERT INTO Studenti (NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES ('Ionascu', 'Ionelia', 6001122390199, 'F', 'Str. Florilor', 203, 8) ;
INSERT INTO Studenti (NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES ('Popesc', 'Maria-Mirabela', 2721231300888, 'F', 'Bd. 22 Decembrie, 2, Bl.5, Sc.B, Ap.21', 509, 9) ;
INSERT INTO Studenti (NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES ('Stroescu', 'Mihaela Oana', 6020719120545, 'F', 'Str. Lenei Nr. 234', 305, 5) ;
INSERT INTO Studenti (NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES ('Nicolae', 'Ana Maria', 2690202200345, 'F', 'Bd. Stroe, 49, Bl.Y3, Sc.A, Ap.97', 402, 7) ;
INSERT INTO Studenti (NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES ('Ciorba', 'Cristina', 2690202200345, 'F', 'Str. Clas, 72, Bl.D3, Sc.1, Ap.3', 403, 10) ;
INSERT INTO Studenti (NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES ('Claudia', 'Mihaela', 2690202200345, 'F', 'Bd. Flak, 13, Bl.K4, Sc.1, Ap.16', 4, 12) ;
INSERT INTO Studenti (NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES ('Torta', 'Andreea', 2690202200345, 'F', 'Str. Plos, 46, Bl.PO7, Sc.7, Ap.34', 301, 8) ;
INSERT INTO Studenti (NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES ('Calmasan', 'Stefan', 5010101380625, 'M', 'Bd. Stefan cel Mare, 1, Bl.DF1, Sc.4, Ap.53', 104, 5) ;
INSERT INTO Studenti (NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES ('Droaba', 'Claudiu', 5010101380625, 'M', 'Bd. Banu, 4, Bl.B4, Sc.1, Ap.1', 108, 6) ;
INSERT INTO Studenti  (NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES ('Gloaba', 'Gicu', 5010101380625, 'M', 'Bd. Groza, 4, Bl.B8, Sc.3, Ap.23', 305, 7) ;
INSERT INTO Studenti (NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Facultate)
	VALUES ('Gliga', 'Dumitru', 5010101380634, 'M', 'Pacurari, 4, Bl.B8, Sc.3, Ap.23', 7) ;
INSERT INTO Studenti (NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Facultate)
	VALUES ('Oriana', 'Andreea', 2110138054122, 'F', 'Tatarasi, 6, Bl.B7, Sc.3, Ap.16', 6) ;
INSERT INTO Studenti (NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Facultate)
	VALUES ('Alina', 'Bucevschi', 1234712333563, 'F', 'Podu Ros, 8, Bl.B7, Sc.1, Ap.45', 9) ;

/* InfoCamere */
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 1, 0, 1);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 2, 0, 2);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 3, 0, 3);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 4, 0, 4);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 5, 0, 5);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 6, 0, 6);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 7, 0, 7);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 8, 0, 8);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 9, 0, 9);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 100, 1, 100);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 101, 1, 101);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 102, 1, 102);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 103, 1, 103);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 104, 1, 104);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 105, 1, 105);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 106, 1, 106);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 107, 1, 107);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 108, 1, 108);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 305, 1, 305);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 109, 1, 109);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 200, 1, 200);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 201, 1, 201);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 202, 1, 202);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 206, 1, 206);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 207, 1, 207);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 208, 1, 208);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 209, 1, 209);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 300, 1, 300);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 301, 1, 301);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 302, 1, 302);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 304, 1, 304);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (4, 306, 1, 306);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (4, 307, 1, 307);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (4, 308, 1, 308);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (4, 309, 1, 309);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (4, 400, 1, 400);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (4, 401, 1, 401);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (4, 402, 1, 402);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 403, 1, 403);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 404, 1, 404);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 405, 1, 405);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 406, 1, 406);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 407, 1, 407);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 408, 1, 408);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 409, 1, 409);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 500, 1, 500);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 501, 1, 501);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 502, 1, 502);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 503, 1, 503);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 504, 1, 504);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 505, 1, 505);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 506, 1, 506);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 507, 1, 507);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 508, 1, 508);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 509, 1, 509);

/* InfoCazari */
INSERT INTO InfoCazari (id_Camin ,id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (1, 1, 1, 199, '2017-02-12', '2018-04-30');
INSERT INTO InfoCazari (id_Camin ,id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (1, 2, 2, 199, '2017-02-12', '2018-04-30');
INSERT INTO InfoCazari (id_Camin ,id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (1, 3, 3, 199, '2017-02-12', '2018-04-30');
INSERT INTO InfoCazari (id_Camin ,id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (1, 4, 4, 199, '2017-02-12', '2018-04-30');
INSERT INTO InfoCazari (id_Camin ,id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (1, 5, 5, 199, '2017-02-12', '2018-04-30');
INSERT INTO InfoCazari (id_Camin ,id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (1, 6, 6, 199, '2017-02-12', '2018-04-30');
INSERT INTO InfoCazari (id_Camin ,id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (1, 7, 7, 199, '2017-02-12', '2018-04-30');
INSERT INTO InfoCazari (id_Camin ,id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (3, 8, 300, 299, '2017-05-19', '2018-07-31');
INSERT INTO InfoCazari (id_Camin ,id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (3, 9, 300, 299, '2017-05-19', '2018-07-31');
INSERT INTO InfoCazari (id_Camin ,id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (4, 10, 402, 399, '2017-05-19', '2018-07-31');
INSERT INTO InfoCazari (id_Camin ,id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (4, 11, 402, 399, '2017-05-19', '2018-07-31');
INSERT INTO InfoCazari (id_Camin ,id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (5, 12, 404, 499, '2017-05-19', '2018-07-31');
INSERT INTO InfoCazari (id_Camin ,id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (5, 13, 404, 499, '2017-05-19', '2018-07-31');
INSERT INTO InfoCazari (id_Camin ,id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (5, 14, 500, 499, '2017-01-01', '2018-09-30');

/* InfoStudenti */
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (1, 1, 3, 7.99);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (1, 2, 1, 6.46);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (1, 3, 6, 7.72);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (1, 4, 6, 8.34);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (1, 5, 8, 6.86);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (1, 6, 9, 5.99);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (1, 7, 5, 8.53);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (3, 8, 7, 7.76);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (3, 9, 10, 8.53);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (4, 10, 12, 8.87);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (4, 11, 8, 8.99);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (5, 12, 5, 9.54);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (5, 13, 6, 9.87);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (5, 14, 7, 9.99);

/* Admini */
CREATE EXTENSION pgcrypto;

COMMENT ON COLUMN Admini.Grad IS '1 = Sef camin ; 2 = Secretara ; 3 = Administrator';
INSERT INTO	Admini (Nume_admin, Email, Username, Parola, Grad) 
	VALUES ('Cristi Popescu', 'CristiP@gmail.com', 'CristiP', MD5('CristiP'), 1);
INSERT INTO	Admini (Nume_admin, Email, Username, Parola, Grad) 
	VALUES ('Grigoras Clara', 'GCL@gmail.com', 'GriClar', MD5('GriClar'), 1);
INSERT INTO	Admini (Nume_admin, Email, Username, Parola, Grad) 
	VALUES ('Pavla Claudiu', 'Claudiu.pavla@gmail.com', 'PavlaClaudiu', MD5('PavlaClaudiu'), 2);
INSERT INTO	Admini (Nume_admin, Email, Username, Parola, Grad) 
	VALUES ('Breton Barbu', 'breton12@gmail.com', 'BretonB', MD5('BretonB'), 2);
INSERT INTO	Admini (Nume_admin, Email, Username, Parola, Grad) 
	VALUES ('Elena Dumitrescu', 'DumiElena@gmail.com', 'DumiElena', MD5('DumiElena'),2);
INSERT INTO	Admini (Nume_admin, Email, Username, Parola, Grad) 
	VALUES ('Danaila Petronela', 'DP46@gmail.com', 'DanaP', MD5('DanaP'),1);
INSERT INTO	Admini (Nume_admin, Email, Username, Parola, Grad) 
	VALUES ('Lupu Ionut', 'Lupuionut@gmail.com', 'LupuI', MD5('LupuI'),1);
INSERT INTO	Admini (Nume_admin, Email, Username, Parola, Grad) 
	VALUES ('Nechita Stanescu', 'Nechi.stan@gmail.com', 'NechiStan', MD5('NechiStan'), 1);
INSERT INTO	Admini (Nume_admin, Email, Username, Parola, Grad) 
	VALUES ('Florea Andreea', 'FloreaAndreea11@gmail.com', 'FloriA', MD5('FloriA'), 1);
INSERT INTO	Admini (Nume_admin, Email, Username, Parola, Grad) 
	VALUES ('Pastorul Marian', 'MarianPastorul@gmail.com', 'Pastorul', MD5('Pastorul'), 1);
INSERT INTO	Admini (Nume_admin, Email, Username, Parola, Grad) 
	VALUES ('Mita Constantin', 'CostiMita@gmail.com', 'Costelus', MD5('Costelus'), 1);
INSERT INTO	Admini (Nume_admin, Email, Username, Parola, Grad) 
	VALUES ('Placinta Dobrea', 'placintica@gmail.com', 'Placintica', MD5('Placintica'), 1);
INSERT INTO	Admini (Nume_admin, Email, Username, Parola, Grad) 
	VALUES ('Alexandru Lapusneanu', 'AlexL@gmail.com', 'AlexL', MD5('AlexL'), 1);
INSERT INTO	Admini (Nume_admin, Email, Username, Parola, Grad) 
	VALUES ('Trabuc Calator', 'Trator15@gmail.com', 'Trator15', MD5('Trator15'), 1);
INSERT INTO	Admini (Nume_admin, Email, Username, Parola, Grad) 
	VALUES ('Muscat Ioana', 'MusIona@gmail.com', 'IonaMus', MD5('IonaMus'), 1);
INSERT INTO	Admini (Nume_admin, Email, Username, Parola, Grad) 
	VALUES ('Tarnacop Ionita', 'Tarnacop@gmail.com', 'Tarnacop', MD5('Tarnacop'), 3);


/* Vizitatori */
INSERT INTO Vizitatori (Nume_Vizitator, CNP_Vizitator, NumeCamin, NrCamera, DataVizitei, OraSosire, OraPlecare)
	VALUES ('Paul Dumitru', '1960316416456', 'C10', '105', '2017-05-05', '12:05:05', '13:05:05');
INSERT INTO Vizitatori (Nume_Vizitator, CNP_Vizitator, NumeCamin, NrCamera, DataVizitei, OraSosire, OraPlecare)
	VALUES ('Mihaita Rapidu', '1940126030937', 'C3', '305', '2017-02-05', '15:35:25', '17:15:05');
INSERT INTO Vizitatori (Nume_Vizitator, CNP_Vizitator, NumeCamin, NrCamera, DataVizitei, OraSosire, OraPlecare)
	VALUES ('Rodica Maricica', '2960316416456', 'C3', '103', '2017-08-12', '07:05:05', '09:55:05');
INSERT INTO Vizitatori (Nume_Vizitator, CNP_Vizitator, NumeCamin, NrCamera, DataVizitei, OraSosire, OraPlecare)
	VALUES ('Capcaun Tiberiu', '1911206528354', 'C13', '505', '2016-03-25', '11:05:05', '13:25:05');
INSERT INTO Vizitatori (Nume_Vizitator, CNP_Vizitator, NumeCamin, NrCamera, DataVizitei, OraSosire, OraPlecare)
	VALUES ('Placinta Dobre', '1981019321411', 'C1', '1', '2018-03-15', '12:05:05', '19:45:05');
INSERT INTO Vizitatori (Nume_Vizitator, CNP_Vizitator, NumeCamin, NrCamera, DataVizitei, OraSosire, OraPlecare)
	VALUES ('Talisman Andra', '2921114118857', 'C3', '302', '2018-07-23', '08:05:05', '22:35:05');
INSERT INTO Vizitatori (Nume_Vizitator, CNP_Vizitator, NumeCamin, NrCamera, DataVizitei, OraSosire, OraPlecare)
	VALUES ('Guster Pamant', '1980609400157', 'C11', '506', '2018-01-01', '00:05:05', '23:58:05');
INSERT INTO Vizitatori (Nume_Vizitator, CNP_Vizitator, NumeCamin, NrCamera, DataVizitei, OraSosire, OraPlecare)
	VALUES ('Vladislav Putin', '1940302368748', 'C5', '405', '2018-06-30', '15:25:05', '15:30:05');
INSERT INTO Vizitatori (Nume_Vizitator, CNP_Vizitator, NumeCamin, NrCamera, DataVizitei, OraSosire, OraPlecare)
	VALUES ('Blanaru Roxana', '2940411209651', 'Akademos', '501', '2018-11-19', '12:05:05', '13:05:05');
INSERT INTO Vizitatori (Nume_Vizitator, CNP_Vizitator, NumeCamin, NrCamera, DataVizitei, OraSosire, OraPlecare)
	VALUES ('Mihaita Boss', '1920822146278', 'Gaudeamus', '407', '2018-02-25', '12:12:05', '18:17:05');


/* Functii */
-- F_NumeCampus - Alegi un camin si-ti afiseaza campusul din care face parte

CREATE OR REPLACE FUNCTION F_NumeCampus (NumeCamin_ Camine.NumeCamin%TYPE)
	RETURNS VARCHAR AS
$$
DECLARE
	v_NumeCampus Campusuri.NumeCampus%TYPE ;
BEGIN
	SELECT NumeCampus INTO v_NumeCampus FROM Campusuri
	WHERE NumeCamin = NumeCamin_ ;
	RETURN v_NumeCampus ;
END ;
$$ LANGUAGE plpgsql; 

-- F_NumeCamin - Alegi un student si-ti afiseaza caminul in care este cazat
CREATE OR REPLACE FUNCTION F_NumeCamin (NumeStud_ Studenti.NumeStud_%TYPE)
	RETURNS VARCHAR AS
$$
DECLARE
	v_NumeCamin Camine.NumeCamin%TYPE ;
BEGIN
	SELECT NumeCamin INTO v_NumeCamin FROM Camine
	WHERE NumeStud = NumeStud_ ;
	RETURN v_NumeCamin ;
END ;
$$ LANGUAGE plpgsql; 


-- F_Camera - Alegi caminul si-ti afiseaza camerele din acel camin
CREATE OR REPLACE FUNCTION F_Camera (id_Camin_ Camine.id_Camin%TYPE)
	RETURNS TABLE (numeCamera Camere.id_Camera%TYPE) AS
$$
BEGIN
	RETURN QUERY
	SELECT id_Camera FROM Camere WHERE id_Camin = id_Camin_ ;
END ;
$$ LANGUAGE plpgsql ;