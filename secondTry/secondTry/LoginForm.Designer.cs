﻿namespace secondTry
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.odbcSelectCommand1 = new System.Data.Odbc.OdbcCommand();
            this.odbcInsertCommand1 = new System.Data.Odbc.OdbcCommand();
            this.odbcUpdateCommand1 = new System.Data.Odbc.OdbcCommand();
            this.odbcDeleteCommand1 = new System.Data.Odbc.OdbcCommand();
            this.odbcDataAdapter1 = new System.Data.Odbc.OdbcDataAdapter();
            this.odbcSelectCommand2 = new System.Data.Odbc.OdbcCommand();
            this.odbcInsertCommand2 = new System.Data.Odbc.OdbcCommand();
            this.odbcUpdateCommand2 = new System.Data.Odbc.OdbcCommand();
            this.odbcDeleteCommand2 = new System.Data.Odbc.OdbcCommand();
            this.odbcDataAdapter2 = new System.Data.Odbc.OdbcDataAdapter();
            this.odbcSelectCommand3 = new System.Data.Odbc.OdbcCommand();
            this.odbcConnection1 = new System.Data.Odbc.OdbcConnection();
            this.odbcDataAdapter3 = new System.Data.Odbc.OdbcDataAdapter();
            this.lbUsername = new System.Windows.Forms.Label();
            this.lbPassword = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.SuspendLayout();
            // 
            // odbcDataAdapter1
            // 
            this.odbcDataAdapter1.DeleteCommand = this.odbcDeleteCommand1;
            this.odbcDataAdapter1.InsertCommand = this.odbcInsertCommand1;
            this.odbcDataAdapter1.SelectCommand = this.odbcSelectCommand1;
            this.odbcDataAdapter1.UpdateCommand = this.odbcUpdateCommand1;
            // 
            // odbcDataAdapter2
            // 
            this.odbcDataAdapter2.DeleteCommand = this.odbcDeleteCommand2;
            this.odbcDataAdapter2.InsertCommand = this.odbcInsertCommand2;
            this.odbcDataAdapter2.SelectCommand = this.odbcSelectCommand2;
            this.odbcDataAdapter2.UpdateCommand = this.odbcUpdateCommand2;
            // 
            // odbcSelectCommand3
            // 
            this.odbcSelectCommand3.CommandText = "select * from camine";
            this.odbcSelectCommand3.Connection = this.odbcConnection1;
            // 
            // odbcConnection1
            // 
            this.odbcConnection1.ConnectionString = resources.GetString("odbcConnection1.ConnectionString");
            // 
            // odbcDataAdapter3
            // 
            this.odbcDataAdapter3.SelectCommand = this.odbcSelectCommand3;
            this.odbcDataAdapter3.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Table", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id_camin", "id_camin"),
                        new System.Data.Common.DataColumnMapping("numecamin", "numecamin"),
                        new System.Data.Common.DataColumnMapping("adresa", "adresa"),
                        new System.Data.Common.DataColumnMapping("nretaje", "nretaje"),
                        new System.Data.Common.DataColumnMapping("nrcamere", "nrcamere"),
                        new System.Data.Common.DataColumnMapping("numeadm", "numeadm"),
                        new System.Data.Common.DataColumnMapping("id_campus", "id_campus")})});
            // 
            // lbUsername
            // 
            this.lbUsername.AutoSize = true;
            this.lbUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUsername.Location = new System.Drawing.Point(33, 30);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(91, 20);
            this.lbUsername.TabIndex = 0;
            this.lbUsername.Text = "Username";
            // 
            // lbPassword
            // 
            this.lbPassword.AutoSize = true;
            this.lbPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPassword.Location = new System.Drawing.Point(33, 83);
            this.lbPassword.Name = "lbPassword";
            this.lbPassword.Size = new System.Drawing.Size(86, 20);
            this.lbPassword.TabIndex = 1;
            this.lbPassword.Text = "Password";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(136, 32);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(139, 20);
            this.txtUsername.TabIndex = 2;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(136, 85);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(139, 20);
            this.txtPassword.TabIndex = 3;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(37, 146);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(99, 38);
            this.btnLogin.TabIndex = 4;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(161, 146);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(99, 38);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(306, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(306, 243);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.lbPassword);
            this.Controls.Add(this.lbUsername);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "LoginForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Data.Odbc.OdbcCommand odbcSelectCommand1;
        private System.Data.Odbc.OdbcCommand odbcInsertCommand1;
        private System.Data.Odbc.OdbcCommand odbcUpdateCommand1;
        private System.Data.Odbc.OdbcCommand odbcDeleteCommand1;
        private System.Data.Odbc.OdbcDataAdapter odbcDataAdapter1;
        private System.Data.Odbc.OdbcCommand odbcSelectCommand2;
        private System.Data.Odbc.OdbcCommand odbcInsertCommand2;
        private System.Data.Odbc.OdbcCommand odbcUpdateCommand2;
        private System.Data.Odbc.OdbcCommand odbcDeleteCommand2;
        private System.Data.Odbc.OdbcDataAdapter odbcDataAdapter2;
        private System.Data.Odbc.OdbcCommand odbcSelectCommand3;
        private System.Data.Odbc.OdbcConnection odbcConnection1;
        private System.Data.Odbc.OdbcDataAdapter odbcDataAdapter3;
        private System.Windows.Forms.Label lbUsername;
        private System.Windows.Forms.Label lbPassword;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.MenuStrip menuStrip1;
    }
}

