﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;

namespace secondTry
{
    public partial class FormUpdateStud : Form
    {
        OdbcConnection conexiune;
        OdbcCommand comanda;
        DataSet dsDateStudenti;
        DataTable tblStudenti
            ;
        OdbcDataReader cititor;
        public FormUpdateStud()
        {
            InitializeComponent();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            conexiune = new OdbcConnection();
            conexiune.ConnectionString = "Driver={PostgreSQL ANSI}; Dsn=PostgreSQL30;database=bazededate7;server=localhost;port=5433;uid=postgres;sslmode=disable;readonly=0;protocol=7.4;fakeoidindex=0;showoidcolumn=0;rowversioning=0;showsystemtables=0;fetch=100;unknownsizes=0;maxvarcharsize=255;maxlongvarcharsize=8190;debug=0;commlog=0;usedeclarefetch=0;textaslongvarchar=1;unknownsaslongvarchar=0;boolsaschar=1;parse=0;extrasystableprefixes=dd_;lfconversion=1;updatablecursors=1;trueisminus1=0;bi=0;byteaaslongvarbinary=0;useserversideprepare=1;lowercaseidentifier=0;gssauthusegss=0;xaopt=1;password=password";
            conexiune.Open();
            comanda = new OdbcCommand();
            comanda.Connection = conexiune;
            comanda.CommandText = "SELECT * FROM Studenti";
            cititor = comanda.ExecuteReader();


            tblStudenti = new DataTable("studenti1");
            tblStudenti.Load(cititor);


            dsDateStudenti = new DataSet();
            dsDateStudenti.Tables.Add(tblStudenti);
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            string id_camera1 = txtcamera.Text;
            string numestud1 = txtnumestud.Text;
            string prenumestud1 = txtprenumestud.Text;
            string cnp1 = txtcnp.Text;
            //string query = "UPDATE  Studenti set id_camera="id_camera1" where numestud="numestud1"and cnp="cnp1"";
            string query = "UPDATE Studenti SET id_camera = '" + id_camera1 + "' WHERE NumeStud = '" + numestud1 + "' and CNP = '" + cnp1 + "'";
           
         
            OdbcCommand update;
            update = new OdbcCommand(query, conexiune);
                     update.ExecuteNonQuery();
            MessageBox.Show("Camera a fost schimbata.");
        }

        private void btnupdatefacultate_Click(object sender, EventArgs e)
        {
            string id_facultate1 = txtfacultate.Text;
            string numestud1 = txtnumestud.Text;
            string cnp1 = txtcnp.Text;
            //string query = "UPDATE  Studenti set id_camera="id_camera1" where numestud="numestud1"and cnp="cnp1"";
            string query = "UPDATE Studenti SET id_facultate = '" + id_facultate1 + "' WHERE NumeStud = '" + numestud1 + "' and CNP = '" + cnp1 + "'";


            OdbcCommand updatef;
            updatef = new OdbcCommand(query, conexiune);
            updatef.ExecuteNonQuery();
            MessageBox.Show("Facultatea a fost schimbata.");
        }

        private void btnUpdatenume_Click(object sender, EventArgs e)
        {
            
            string numestud1 = txtnumestud.Text;
            string prenumestud1 = txtprenumestud.Text;
            string cnp1 = txtcnp.Text;
            //string query = "UPDATE  Studenti set id_camera="id_camera1" where numestud="numestud1"and cnp="cnp1"";
            string query = "UPDATE Studenti SET numestud = '" + numestud1 + "' WHERE prenumestud = '" + prenumestud1 + "' and CNP = '" + cnp1 + "'";


            OdbcCommand updaten;
            updaten = new OdbcCommand(query, conexiune);
            updaten.ExecuteNonQuery();
            MessageBox.Show("Numele de familie a fost schimbat.");
        }
    }
}
