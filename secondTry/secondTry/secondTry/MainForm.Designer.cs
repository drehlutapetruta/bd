﻿namespace secondTry
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mnSefCamin = new System.Windows.Forms.ToolStripMenuItem();
            this.mnVizitatori = new System.Windows.Forms.ToolStripMenuItem();
            this.mnSecretara = new System.Windows.Forms.ToolStripMenuItem();
            this.adaugareStudentiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adaugareCaminToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formComplexCaminToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adaugareFacultateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formSimpluComplexCaminToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnAdministrator = new System.Windows.Forms.ToolStripMenuItem();
            this.adaugareUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restantieriPlataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adaugareChiriiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateStudentiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnSefCamin,
            this.mnSecretara,
            this.mnAdministrator});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(837, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // mnSefCamin
            // 
            this.mnSefCamin.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnVizitatori});
            this.mnSefCamin.Name = "mnSefCamin";
            this.mnSefCamin.Size = new System.Drawing.Size(73, 20);
            this.mnSefCamin.Text = "Sef Camin";
            this.mnSefCamin.Click += new System.EventHandler(this.studentiFormToolStripMenuItem_Click);
            // 
            // mnVizitatori
            // 
            this.mnVizitatori.Name = "mnVizitatori";
            this.mnVizitatori.Size = new System.Drawing.Size(180, 22);
            this.mnVizitatori.Text = "Vizitatori";
            this.mnVizitatori.Click += new System.EventHandler(this.mnVizitatori_Click);
            // 
            // mnSecretara
            // 
            this.mnSecretara.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adaugareStudentiToolStripMenuItem,
            this.adaugareCaminToolStripMenuItem,
            this.formComplexCaminToolStripMenuItem,
            this.adaugareFacultateToolStripMenuItem,
            this.formSimpluComplexCaminToolStripMenuItem,
            this.restantieriPlataToolStripMenuItem,
            this.adaugareChiriiToolStripMenuItem,
            this.updateStudentiToolStripMenuItem});
            this.mnSecretara.Name = "mnSecretara";
            this.mnSecretara.Size = new System.Drawing.Size(67, 20);
            this.mnSecretara.Text = "Secretara";
            // 
            // adaugareStudentiToolStripMenuItem
            // 
            this.adaugareStudentiToolStripMenuItem.Name = "adaugareStudentiToolStripMenuItem";
            this.adaugareStudentiToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.adaugareStudentiToolStripMenuItem.Text = "Adaugare Studenti";
            this.adaugareStudentiToolStripMenuItem.Click += new System.EventHandler(this.adaugareStudentiToolStripMenuItem_Click);
            // 
            // adaugareCaminToolStripMenuItem
            // 
            this.adaugareCaminToolStripMenuItem.Name = "adaugareCaminToolStripMenuItem";
            this.adaugareCaminToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.adaugareCaminToolStripMenuItem.Text = "Informatii campusuri";
            this.adaugareCaminToolStripMenuItem.Click += new System.EventHandler(this.adaugareCaminToolStripMenuItem_Click);
            // 
            // formComplexCaminToolStripMenuItem
            // 
            this.formComplexCaminToolStripMenuItem.Name = "formComplexCaminToolStripMenuItem";
            this.formComplexCaminToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.formComplexCaminToolStripMenuItem.Text = "FormComplexCamin";
            this.formComplexCaminToolStripMenuItem.Click += new System.EventHandler(this.formComplexCaminToolStripMenuItem_Click);
            // 
            // adaugareFacultateToolStripMenuItem
            // 
            this.adaugareFacultateToolStripMenuItem.Name = "adaugareFacultateToolStripMenuItem";
            this.adaugareFacultateToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.adaugareFacultateToolStripMenuItem.Text = "Adaugare Facultate";
            this.adaugareFacultateToolStripMenuItem.Click += new System.EventHandler(this.adaugareFacultateToolStripMenuItem_Click);
            // 
            // formSimpluComplexCaminToolStripMenuItem
            // 
            this.formSimpluComplexCaminToolStripMenuItem.Name = "formSimpluComplexCaminToolStripMenuItem";
            this.formSimpluComplexCaminToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.formSimpluComplexCaminToolStripMenuItem.Text = "FormSimpluComplexCamin";
            this.formSimpluComplexCaminToolStripMenuItem.Click += new System.EventHandler(this.formSimpluComplexCaminToolStripMenuItem_Click);
            // 
            // mnAdministrator
            // 
            this.mnAdministrator.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adaugareUserToolStripMenuItem});
            this.mnAdministrator.Name = "mnAdministrator";
            this.mnAdministrator.Size = new System.Drawing.Size(92, 20);
            this.mnAdministrator.Text = "Administrator";
            // 
            // adaugareUserToolStripMenuItem
            // 
            this.adaugareUserToolStripMenuItem.Name = "adaugareUserToolStripMenuItem";
            this.adaugareUserToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.adaugareUserToolStripMenuItem.Text = "Adaugare User";
            this.adaugareUserToolStripMenuItem.Click += new System.EventHandler(this.adaugareUserToolStripMenuItem_Click);
            // 
            // restantieriPlataToolStripMenuItem
            // 
            this.restantieriPlataToolStripMenuItem.Name = "restantieriPlataToolStripMenuItem";
            this.restantieriPlataToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.restantieriPlataToolStripMenuItem.Text = "Restantieri plata";
            this.restantieriPlataToolStripMenuItem.Click += new System.EventHandler(this.restantieriPlataToolStripMenuItem_Click);
            // 
            // adaugareChiriiToolStripMenuItem
            // 
            this.adaugareChiriiToolStripMenuItem.Name = "adaugareChiriiToolStripMenuItem";
            this.adaugareChiriiToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.adaugareChiriiToolStripMenuItem.Text = "Adaugare Chirii";
            this.adaugareChiriiToolStripMenuItem.Click += new System.EventHandler(this.adaugareChiriiToolStripMenuItem_Click);
            // 
            // updateStudentiToolStripMenuItem
            // 
            this.updateStudentiToolStripMenuItem.Name = "updateStudentiToolStripMenuItem";
            this.updateStudentiToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.updateStudentiToolStripMenuItem.Text = "Update Studenti";
            this.updateStudentiToolStripMenuItem.Click += new System.EventHandler(this.updateStudentiToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 433);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnSefCamin;
        private System.Windows.Forms.ToolStripMenuItem mnVizitatori;
        private System.Windows.Forms.ToolStripMenuItem mnSecretara;
        private System.Windows.Forms.ToolStripMenuItem mnAdministrator;
        private System.Windows.Forms.ToolStripMenuItem adaugareStudentiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adaugareUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adaugareCaminToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formComplexCaminToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adaugareFacultateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formSimpluComplexCaminToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restantieriPlataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adaugareChiriiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateStudentiToolStripMenuItem;
    }
}