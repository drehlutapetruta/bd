﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;

namespace secondTry
{
    public partial class FormSimpluComplexCamin : Form
    {
        OdbcConnection conexiune;
        DataSet dsDate;
        int indexInregistrareCurenta;
        string tipOperatiune;

        public FormSimpluComplexCamin()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.DeschideConexiunea();
            this.afisareInregistrareCurenta(indexInregistrareCurenta);
            this.dezactiveazaCaseteText();
            btnSalvare.Visible = false;
            btnAnulare.Visible = false;
            populeazaCamine();

        }
        private void DeschideConexiunea()
        {
            try
            {

                conexiune = new OdbcConnection();

                conexiune.ConnectionString = "Driver={PostgreSQL ANSI}; Dsn=PostgreSQL30;database=CampusuriProiect;server=localhost;port=5432;uid=postgres;sslmode=disable;readonly=0;protocol=7.4;fakeoidindex=0;showoidcolumn=0;rowversioning=0;showsystemtables=0;fetch=100;unknownsizes=0;maxvarcharsize=255;maxlongvarcharsize=8190;debug=0;commlog=0;usedeclarefetch=0;textaslongvarchar=1;unknownsaslongvarchar=0;boolsaschar=1;parse=0;extrasystableprefixes=dd_;lfconversion=1;updatablecursors=1;trueisminus1=0;bi=0;byteaaslongvarbinary=0;useserversideprepare=1;lowercaseidentifier=0;gssauthusegss=0;xaopt=1;password=password";
                //deschid conexiunea
                conexiune.Open();
                OdbcCommand comanda;
                comanda = new OdbcCommand();
                comanda.Connection = conexiune;
                comanda.CommandText = "SELECT * FROM camine";

                OdbcDataReader cititor;
                cititor = comanda.ExecuteReader();

                DataTable tblCamine;

                tblCamine = new DataTable("CAMINE");
                tblCamine.Load(cititor);


                dsDate = new DataSet();
                dsDate.Tables.Add(tblCamine);

                //nrTotalInregistrari = dsDate.Tables["Camine"].Rows.Count;

                this.indexInregistrareCurenta = 0;

                dataGridView1.DataSource = dsDate;
                dataGridView1.DataMember = "Camine";
                dataGridView1.Refresh();
            }
            catch (OdbcException eroare)
            {
                MessageBox.Show("A aparut eroarea nr. " + eroare.ErrorCode.ToString() + " cu mesajul " +
                eroare.Message.ToString());
                if (conexiune.State == ConnectionState.Open)
                    conexiune.Close();
            }
        }
        private void populeazaCamine()
        {
            OdbcCommand comanda;
            comanda = new OdbcCommand();
            comanda.Connection = conexiune;
            comanda.CommandText = "SELECT * FROM camine ORDER BY id_camin";

            OdbcDataReader cititor;
            cititor = comanda.ExecuteReader();

            DataTable tblCampusuri;
            tblCampusuri = new DataTable("Campusuri");
            tblCampusuri.Load(cititor);

            dsDate.Tables.Add(tblCampusuri);
            //incerc sa populez combobox-ul
            comboBox1.DataSource = dsDate.Tables["Campusuri"];
            comboBox1.DisplayMember = "Id_Campus";
            comboBox1.ValueMember = "id_campus";
        }
        private void afisareInregistrareCurenta(int nrInregistrare)
        {

            textBox1.Text = dsDate.Tables["Camine"].Rows[nrInregistrare].ItemArray[0].ToString();
            textBox2.Text = dsDate.Tables["Camine"].Rows[nrInregistrare].ItemArray[1].ToString();
            textBox3.Text = dsDate.Tables["Camine"].Rows[nrInregistrare].ItemArray[2].ToString();
            textBox4.Text = dsDate.Tables["Camine"].Rows[nrInregistrare].ItemArray[3].ToString();
            textBox5.Text = dsDate.Tables["Camine"].Rows[nrInregistrare].ItemArray[4].ToString();
            textBox6.Text = dsDate.Tables["Camine"].Rows[nrInregistrare].ItemArray[5].ToString();
            textBox7.Text = dsDate.Tables["Camine"].Rows[nrInregistrare].ItemArray[6].ToString();

        }

        private void dezactiveazaCaseteText()
        {
            textBox1.Enabled = false;
            textBox2.Enabled = false;
            textBox3.Enabled = false;
            textBox4.Enabled = false;
            textBox5.Enabled = false;
            textBox6.Enabled = false;
            textBox7.Enabled = false;
        }
        private void activeazaCaseteText()
        {
            textBox1.Enabled = true;
            textBox2.Enabled = true;
            textBox3.Enabled = true;
            textBox4.Enabled = true;
            textBox5.Enabled = true;
            textBox6.Enabled = true;
            textBox7.Enabled = true;
        }
       
        private void dezactiveazaNavigare()
        {
            btnAnterior.Visible = false;
            btnUrmator.Visible = false;
            btnPrimul.Visible = false;
            btnUltimul.Visible = false;
        }
        private void activeazaNavigare()
        {
            btnAnterior.Visible = true;
            btnUrmator.Visible = true;
            btnPrimul.Visible = true;
            btnUltimul.Visible = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnModifica_Click_1(object sender, EventArgs e)
        {

            tipOperatiune = "modificare";
            dezactiveazaNavigare();
            activeazaCaseteText();
            btnAdaugare.Visible = false;
            btnStergere.Visible = false;
            btnModifica.Visible = false;
            btnSalvare.Visible = true;
            btnAnulare.Visible = true;
            textBox1.Enabled = false;
        }

        private void btnUrmator_Click_1(object sender, EventArgs e)
        {
            if (indexInregistrareCurenta == dsDate.Tables["camine"].Rows.Count - 1)
                indexInregistrareCurenta = 0;
            else
                indexInregistrareCurenta++;

            afisareInregistrareCurenta(indexInregistrareCurenta);
        }

        private void btnAnterior_Click_1(object sender, EventArgs e)
        {
            if (indexInregistrareCurenta == 0)
                indexInregistrareCurenta = dsDate.Tables["camine"].Rows.Count - 1;

            else
                indexInregistrareCurenta--;

            afisareInregistrareCurenta(indexInregistrareCurenta);
        }

        private void btnPrimul_Click_1(object sender, EventArgs e)
        {
            indexInregistrareCurenta = 0;
            afisareInregistrareCurenta(indexInregistrareCurenta);
        }

        private void btnUltimul_Click_1(object sender, EventArgs e)
        {
            indexInregistrareCurenta = dsDate.Tables["camine"].Rows.Count - 1;
            afisareInregistrareCurenta(indexInregistrareCurenta);
        }

        private void btnAdaugare_Click_1(object sender, EventArgs e)
        {
            tipOperatiune = "adaugare";
            activeazaCaseteText();
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox1.Focus();

            btnAdaugare.Visible = false;
            btnSalvare.Visible = true;
            btnAnulare.Visible = true;
            btnStergere.Visible = false;

            dezactiveazaNavigare();
            textBox7.Visible = false;
            comboBox1.Visible = true;
            btnModifica.Visible = false;

        }

        private void btnSalvare_Click_1(object sender, EventArgs e)
        {
            if (tipOperatiune == "adaugare")
            {
                try
                {
                    //inserez in baza de date inregistrarea curenta
                    OdbcCommand comanda = new OdbcCommand();
                    comanda.CommandText = "INSERT INTO camine VALUES(?,?,?,?,?,?,?)";
                    comanda.Connection = conexiune;
                    //stabilim valorile parametrilor in functie de ce a introdus utilizatorul
                    comanda.Parameters.AddWithValue("", textBox1.Text.ToString());
                    comanda.Parameters.AddWithValue("", textBox2.Text.ToString());
                    comanda.Parameters.AddWithValue("", textBox3.Text.ToString());
                    comanda.Parameters.AddWithValue("", textBox4.Text.ToString());
                    comanda.Parameters.AddWithValue("", textBox5.Text.ToString());
                    comanda.Parameters.AddWithValue("", textBox6.Text.ToString());

                    comanda.Parameters.AddWithValue("", comboBox1.SelectedValue.ToString());
                    comanda.ExecuteNonQuery();

                    MessageBox.Show("Caminul a fost adaugat in baza de date!");
                    activeazaNavigare();
                    dezactiveazaCaseteText();
                    btnAdaugare.Visible = true;
                    btnSalvare.Visible = false;
                    btnAnulare.Visible = false;
                    btnStergere.Visible = true;
                    textBox7.Visible = true;
                    comboBox1.Visible = false;
                    btnModifica.Visible = true;
                    DeschideConexiunea();
                    afisareInregistrareCurenta(indexInregistrareCurenta);
                }
                catch (OdbcException eroare)
                {
                    MessageBox.Show("A aparut eroarea nr. " + eroare.ErrorCode.ToString() + " cu mesajul " +
                    eroare.Message.ToString());
                }
            }
            if (tipOperatiune == "modificare")
            {
                try
                {
                    OdbcCommand comanda = new OdbcCommand();
                    comanda.Connection = conexiune;
                    comanda.CommandText = "UPDATE camine SET id_camin=?, numecamin=?, adresa=?, nretaje=?, nrcamere=?, numeadm=?, id_campus=? WHERE id_camin =? ";

                    comanda.Parameters.AddWithValue("", textBox2.Text.ToString());
                    comanda.Parameters.AddWithValue("", textBox3.Text.ToString());
                    comanda.Parameters.AddWithValue("", textBox4.Text.ToString());
                    comanda.Parameters.AddWithValue("", textBox5.Text.ToString());
                    comanda.Parameters.AddWithValue("", textBox6.Text.ToString());
                    comanda.Parameters.AddWithValue("", textBox7.Text.ToString());
                    comanda.Parameters.AddWithValue("", textBox1.Text.ToString());

                    comanda.ExecuteNonQuery();
                    MessageBox.Show("Am modificat caminul");
                    activeazaNavigare();
                    dezactiveazaCaseteText();
                    btnAdaugare.Visible = true;
                    btnSalvare.Visible = false;
                    btnAnulare.Visible = false;
                    btnStergere.Visible = false;

                    btnModifica.Visible = true;
                }
                catch (OdbcException eroare)
                {
                    MessageBox.Show("A aparut eroarea nr. " + eroare.ErrorCode.ToString() + " cu mesajul " +
                    eroare.Message.ToString());
                }
            }
            }

        private void btnAnulare_Click_1(object sender, EventArgs e)
        {
            activeazaNavigare();
            dezactiveazaCaseteText();
            btnAnulare.Visible = false;
            btnSalvare.Visible = false;
            btnAdaugare.Visible = true;
            btnStergere.Visible = true;

            btnModifica.Visible = true;
            afisareInregistrareCurenta(indexInregistrareCurenta);
            textBox7.Visible = true;
            comboBox1.Visible = false;
        }

        private void btnStergere_Click_1(object sender, EventArgs e)
        {
            //incerc stergerea, dupa confirmarea de la utilizator
            if (MessageBox.Show("Sigur stergi caminul curent?", "Decizie", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    OdbcCommand comanda = new OdbcCommand();
                    comanda.CommandText = "DELETE FROM camine WHERE id_camin=?";
                    comanda.Parameters.AddWithValue("id_camin", textBox1.Text.ToString());
                    comanda.Connection = conexiune;
                    comanda.ExecuteNonQuery();
                    MessageBox.Show("Am sters caminul");
                    DeschideConexiunea();
                    afisareInregistrareCurenta(indexInregistrareCurenta);
                }
                catch (OdbcException eroare)
                {
                    MessageBox.Show("A aparut eroarea nr. " + eroare.ErrorCode.ToString() + " cu mesajul " +
                    eroare.Message.ToString());
                }
            }
        }

        private void btnRefresh_Click_1(object sender, EventArgs e)
        {
            conexiune.Close();
            conexiune.Open();
        }
    }
}

