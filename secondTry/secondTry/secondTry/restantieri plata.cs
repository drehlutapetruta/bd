﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;

namespace secondTry
{
    public partial class restantieri_plata : Form

    {
        OdbcConnection conexiune;
        OdbcCommand comanda;
        DataSet dsDateRestantieriPlataa;
        DataSet dsDateCamine;
        DataTable tblRestantieriPlataa ;
        DataTable tblCamine;
        OdbcDataReader cititor;
        public restantieri_plata()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            DataRowView nume = (DataRowView)comboBox1.SelectedItem;


            string combobox1 = nume["numecamin"].ToString();
            comanda = new OdbcCommand();
            comanda.CommandText = "select distinct * from RestantieriPlata r inner join Camine c on r.id_camin = c.id_camin  where numecamin =?";
            comanda.Connection = conexiune;
            comanda.Parameters.Clear();
            comanda.Parameters.AddWithValue("numecamin", combobox1);
            OdbcDataReader cititor;
            cititor = comanda.ExecuteReader();
            DataTable tblRestantieriPlata;
            tblRestantieriPlata = new DataTable("Restantieri");
            tblRestantieriPlata.Load(cititor);
            dsDateRestantieriPlataa = new DataSet();
            dsDateRestantieriPlataa.Tables.Add(tblRestantieriPlata);


            dataGridView1.DataSource = dsDateRestantieriPlataa;
            dataGridView1.DataMember = "Facultate";
            dataGridView1.Refresh();
        }

        private void restantieri_plata_Load(object sender, EventArgs e)
        {
            conexiune = new OdbcConnection();
            conexiune.ConnectionString = "Driver={PostgreSQL ANSI}; Dsn=PostgreSQL30;database=bazededate7;server=localhost;port=5433;uid=postgres;sslmode=disable;readonly=0;protocol=7.4;fakeoidindex=0;showoidcolumn=0;rowversioning=0;showsystemtables=0;fetch=100;unknownsizes=0;maxvarcharsize=255;maxlongvarcharsize=8190;debug=0;commlog=0;usedeclarefetch=0;textaslongvarchar=1;unknownsaslongvarchar=0;boolsaschar=1;parse=0;extrasystableprefixes=dd_;lfconversion=1;updatablecursors=1;trueisminus1=0;bi=0;byteaaslongvarbinary=0;useserversideprepare=1;lowercaseidentifier=0;gssauthusegss=0;xaopt=1;password=password";
            conexiune.Open();
            comanda = new OdbcCommand();
            comanda.Connection = conexiune;
            comanda.CommandText = "SELECT * FROM RestantieriPlata";
            
            cititor = comanda.ExecuteReader();
            

            tblRestantieriPlataa = new DataTable("RestantieriPlataa");
            tblRestantieriPlataa.Load(cititor);


            dsDateRestantieriPlataa = new DataSet();
            dsDateRestantieriPlataa.Tables.Add(tblRestantieriPlataa);
            

           tblCamine = new DataTable("Camine");
            tblCamine.Load(cititor);


            dsDateCamine = new DataSet();
            dsDateCamine.Tables.Add(tblCamine);



            //conexiune = new OdbcConnection();
            //conexiune.ConnectionString = "Driver={PostgreSQL ANSI};database=CampusuriProiect;server=localhost;port=5432;uid=postgres;pwd=password;sslmode=disable;readonly=0;protocol=7.4;fakeoidindex=0;showoidcolumn=0;rowversioning=0;showsystemtables=0;fetch=100;unknownsizes=0;maxvarcharsize=255;maxlongvarcharsize=8190;debug=0;commlog=0;usedeclarefetch=0;textaslongvarchar=1;unknownsaslongvarchar=0;boolsaschar=1;parse=0;lfconversion=1;updatablecursors=1;trueisminus1=0;bi=0;byteaaslongvarbinary=1;useserversideprepare=1;lowercaseidentifier=0;xaopt=1;";
            //conexiune.Open();
            //comanda = new OdbcCommand();
            //comanda.Connection = conexiune;
            //comanda.CommandText = " Select * from camine";

            //cititor = comanda.ExecuteReader();
            //DataTable tblCamine;
            //tblCamine = new DataTable("camine");
            //tblCamine.Load(cititor);
            //dsDateCamin = new DataSet();
            //dsDateCamin.Tables.Add(tblCamine);
            comboBox1.DataSource = dsDateCamine.Tables["camine"];
            comboBox1.DisplayMember = "numecamin";
            
            comboBox1.Refresh();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
    }
}
