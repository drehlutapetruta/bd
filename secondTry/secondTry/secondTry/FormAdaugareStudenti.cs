﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;

namespace secondTry
{
    public partial class FormAdaugareStudenti : Form
    {
        OdbcConnection conexiune;
        OdbcCommand comanda;
        DataSet dsDate;
        DataTable tblStudenti;
        OdbcDataReader cititor;
        int inregistrareCurenta;


        public FormAdaugareStudenti()
        {
            InitializeComponent();
        }

        private void AfiseazaInregistrareCurenta(int i)
        {
            textBox1.Text = dsDate.Tables["Studenti"].Rows[i].ItemArray[0].ToString();
            textBox2.Text = dsDate.Tables["Studenti"].Rows[i].ItemArray[1].ToString();
            textBox3.Text = dsDate.Tables["Studenti"].Rows[i].ItemArray[2].ToString();
            textBox4.Text = dsDate.Tables["Studenti"].Rows[i].ItemArray[3].ToString();
            textBox5.Text = dsDate.Tables["Studenti"].Rows[i].ItemArray[4].ToString();
            textBox6.Text = dsDate.Tables["Studenti"].Rows[i].ItemArray[5].ToString();
            textBox7.Text = dsDate.Tables["Studenti"].Rows[i].ItemArray[6].ToString();
            textBox8.Text = dsDate.Tables["Studenti"].Rows[i].ItemArray[7].ToString();
        }


        private void Form1_Load_1(object sender, EventArgs e)
        {
            conexiune = new OdbcConnection();
            string connstring = "Driver={PostgreSQL ANSI}; database = Practica; server = localhost; port = 5432; uid = postgres; sslmode = disable; readonly= 0; protocol = 7.4; fakeoidindex = 0; showoidcolumn = 0; rowversioning = 0; showsystemtables = 0; fetch = 100; socket = 4096; unknownsizes = 0; maxvarcharsize = 255; maxlongvarcharsize = 8190; debug = 0; commlog = 0; optimizer = 0; ksqo = 1; usedeclarefetch = 0; textaslongvarchar = 1; unknownsaslongvarchar = 0; boolsaschar = 1; parse = 0; cancelasfreestmt = 0; extrasystableprefixes = dd_; lfconversion = 1; updatablecursors = 1; disallowpremature = 0; trueisminus1 = 0; bi = 0; byteaaslongvarbinary = 0; useserversideprepare = 0; lowercaseidentifier = 0; gssauthusegss = 0; xaopt = 1; pwd = password;";
            conexiune.Open();
            comanda = new OdbcCommand();
            comanda.Connection = conexiune;
            comanda.CommandText = "SELECT * FROM studenti";
            cititor = comanda.ExecuteReader();


            tblStudenti = new DataTable("studenti");
            tblStudenti.Load(cititor);


            dsDate = new DataSet();
            dsDate.Tables.Add(tblStudenti);
            AfiseazaInregistrareCurenta(inregistrareCurenta);

        }

        private void btnAdaugare_Click_1(object sender, EventArgs e)
        {
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            btnSalvare.Visible = true;
            btnAdaugare.Visible = false;
            btnUrmator.Visible = false;
            btnAnterior.Visible = false;
            btnPrimul.Visible = false;
            btnUltim.Visible = false;
            btnAnulare.Visible = true;
            btnStergere.Visible = false;
            textBox1.Visible = false;
            label1.Visible = false;


        }

        private void btnStergere_Click_1(object sender, EventArgs e)
        {
            
            comanda.CommandText = "delete from studenti where id_stud =?";
            comanda.Parameters.Clear();
            comanda.Parameters.AddWithValue("id_stud", textBox1.Text);
            int nrInregAfect = comanda.ExecuteNonQuery();
            if (nrInregAfect == 0)
                MessageBox.Show("Nu  s-a sters nicio inregistrare!");
            else
            {
                MessageBox.Show("Felicitari! Ai sters un student din baza de date!");
                inregistrareCurenta++;
                AfiseazaInregistrareCurenta(inregistrareCurenta);
            }

        }

        private void btnSalvare_Click_1(object sender, EventArgs e)
        {

            try
            {
                comanda.CommandText = "insert into studenti values(?,?,?,?,?,?,?) ";
                comanda.Parameters.Clear();
                comanda.Parameters.AddWithValue("", textBox2.Text);
                comanda.Parameters.AddWithValue("", textBox3.Text);
                comanda.Parameters.AddWithValue("", textBox4.Text);
                comanda.Parameters.AddWithValue("", textBox5.Text);
                comanda.Parameters.AddWithValue("", textBox6.Text);
                comanda.Parameters.AddWithValue("", textBox7.Text);
                comanda.Parameters.AddWithValue("", textBox8.Text);
                comanda.ExecuteNonQuery();
                MessageBox.Show("Felicitari! Ai adaugat cu succes un student!");
                btnSalvare.Visible = false;
                btnAdaugare.Visible = true;
                btnUrmator.Visible = true;
                btnAnterior.Visible = true;
                btnPrimul.Visible = true;
                btnUltim.Visible = true;
                btnAnulare.Visible = false;
                btnStergere.Visible = true;

            }
            catch (Exception eroare)
            {
                MessageBox.Show("Eroarea: " + eroare.Message);
            }


        }

        private void btnAnulare_Click_1(object sender, EventArgs e)
        {
            btnSalvare.Visible = false;
            btnAdaugare.Visible = true;
            btnUrmator.Visible = true;
            btnAnterior.Visible = true;
            btnPrimul.Visible = true;
            btnUltim.Visible = true;
            btnAnulare.Visible = false;
            btnStergere.Visible = true;
            label1.Visible = true;
            textBox1.Visible = true;

            AfiseazaInregistrareCurenta(inregistrareCurenta);

        }

        private void btnPrimul_Click_1(object sender, EventArgs e)
        {
            inregistrareCurenta = 0;
            AfiseazaInregistrareCurenta(inregistrareCurenta);

        }

        private void btnAnterior_Click_1(object sender, EventArgs e)
        {
            if (inregistrareCurenta == 0)
                inregistrareCurenta = dsDate.Tables["studenti"].Rows.Count - 1;
            else
                inregistrareCurenta--;
            AfiseazaInregistrareCurenta(inregistrareCurenta);
        }


        private void btnUrmator_Click_1(object sender, EventArgs e)
        {
            if (inregistrareCurenta >= dsDate.Tables["studenti"].Rows.Count - 1)
                inregistrareCurenta = 0;
            else
                inregistrareCurenta++;
            AfiseazaInregistrareCurenta(inregistrareCurenta);

        }

        private void btnUltim_Click_1(object sender, EventArgs e)
        {

        inregistrareCurenta = dsDate.Tables["studenti"].Rows.Count - 1;
        AfiseazaInregistrareCurenta(inregistrareCurenta);

        }

    }
}
