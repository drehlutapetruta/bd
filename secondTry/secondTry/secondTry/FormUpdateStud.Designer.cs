﻿namespace secondTry
{
    partial class FormUpdateStud
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnupdate = new System.Windows.Forms.Button();
            this.txtnumestud = new System.Windows.Forms.TextBox();
            this.txtprenumestud = new System.Windows.Forms.TextBox();
            this.txtcnp = new System.Windows.Forms.TextBox();
            this.txtcamera = new System.Windows.Forms.TextBox();
            this.txtfacultate = new System.Windows.Forms.TextBox();
            this.btnupdatefacultate = new System.Windows.Forms.Button();
            this.btnUpdatenume = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnupdate
            // 
            this.btnupdate.Location = new System.Drawing.Point(573, 50);
            this.btnupdate.Name = "btnupdate";
            this.btnupdate.Size = new System.Drawing.Size(163, 54);
            this.btnupdate.TabIndex = 0;
            this.btnupdate.Text = "Update camera";
            this.btnupdate.UseVisualStyleBackColor = true;
            this.btnupdate.Click += new System.EventHandler(this.btnupdate_Click);
            // 
            // txtnumestud
            // 
            this.txtnumestud.Location = new System.Drawing.Point(144, 34);
            this.txtnumestud.Name = "txtnumestud";
            this.txtnumestud.Size = new System.Drawing.Size(125, 20);
            this.txtnumestud.TabIndex = 1;
            // 
            // txtprenumestud
            // 
            this.txtprenumestud.Location = new System.Drawing.Point(144, 84);
            this.txtprenumestud.Name = "txtprenumestud";
            this.txtprenumestud.Size = new System.Drawing.Size(125, 20);
            this.txtprenumestud.TabIndex = 2;
            // 
            // txtcnp
            // 
            this.txtcnp.Location = new System.Drawing.Point(144, 146);
            this.txtcnp.Name = "txtcnp";
            this.txtcnp.Size = new System.Drawing.Size(125, 20);
            this.txtcnp.TabIndex = 3;
            // 
            // txtcamera
            // 
            this.txtcamera.Location = new System.Drawing.Point(144, 212);
            this.txtcamera.Name = "txtcamera";
            this.txtcamera.Size = new System.Drawing.Size(125, 20);
            this.txtcamera.TabIndex = 4;
            this.txtcamera.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // txtfacultate
            // 
            this.txtfacultate.Location = new System.Drawing.Point(144, 279);
            this.txtfacultate.Name = "txtfacultate";
            this.txtfacultate.Size = new System.Drawing.Size(125, 20);
            this.txtfacultate.TabIndex = 5;
            // 
            // btnupdatefacultate
            // 
            this.btnupdatefacultate.Location = new System.Drawing.Point(573, 146);
            this.btnupdatefacultate.Name = "btnupdatefacultate";
            this.btnupdatefacultate.Size = new System.Drawing.Size(163, 53);
            this.btnupdatefacultate.TabIndex = 6;
            this.btnupdatefacultate.Text = "Update facultate";
            this.btnupdatefacultate.UseVisualStyleBackColor = true;
            this.btnupdatefacultate.Click += new System.EventHandler(this.btnupdatefacultate_Click);
            // 
            // btnUpdatenume
            // 
            this.btnUpdatenume.Location = new System.Drawing.Point(573, 241);
            this.btnUpdatenume.Name = "btnUpdatenume";
            this.btnUpdatenume.Size = new System.Drawing.Size(163, 58);
            this.btnUpdatenume.TabIndex = 7;
            this.btnUpdatenume.Text = "Update nume de familie";
            this.btnUpdatenume.UseVisualStyleBackColor = true;
            this.btnUpdatenume.Click += new System.EventHandler(this.btnUpdatenume_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Nume";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Prenume";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "CNP";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Camera";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(43, 279);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Facultate";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnUpdatenume);
            this.Controls.Add(this.btnupdatefacultate);
            this.Controls.Add(this.txtfacultate);
            this.Controls.Add(this.txtcamera);
            this.Controls.Add(this.txtcnp);
            this.Controls.Add(this.txtprenumestud);
            this.Controls.Add(this.txtnumestud);
            this.Controls.Add(this.btnupdate);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnupdate;
        private System.Windows.Forms.TextBox txtnumestud;
        private System.Windows.Forms.TextBox txtprenumestud;
        private System.Windows.Forms.TextBox txtcnp;
        private System.Windows.Forms.TextBox txtcamera;
        private System.Windows.Forms.TextBox txtfacultate;
        private System.Windows.Forms.Button btnupdatefacultate;
        private System.Windows.Forms.Button btnUpdatenume;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}