﻿namespace secondTry
{
    partial class VizitatoriForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNume = new System.Windows.Forms.Label();
            this.lblCamera = new System.Windows.Forms.Label();
            this.lblCamin = new System.Windows.Forms.Label();
            this.lblCNP = new System.Windows.Forms.Label();
            this.txtNume = new System.Windows.Forms.TextBox();
            this.txtCNP = new System.Windows.Forms.TextBox();
            this.cmbCamin = new System.Windows.Forms.ComboBox();
            this.cmbCamera = new System.Windows.Forms.ComboBox();
            this.btnAdauga = new System.Windows.Forms.Button();
            this.btnVizitatori = new System.Windows.Forms.Button();
            this.lstVizitatori = new System.Windows.Forms.ListView();
            this.id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Nume = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CNP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.OraSosire = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lblNume
            // 
            this.lblNume.AutoSize = true;
            this.lblNume.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNume.Location = new System.Drawing.Point(41, 40);
            this.lblNume.Name = "lblNume";
            this.lblNume.Size = new System.Drawing.Size(52, 18);
            this.lblNume.TabIndex = 0;
            this.lblNume.Text = "Nume";
            // 
            // lblCamera
            // 
            this.lblCamera.AutoSize = true;
            this.lblCamera.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamera.Location = new System.Drawing.Point(41, 134);
            this.lblCamera.Name = "lblCamera";
            this.lblCamera.Size = new System.Drawing.Size(67, 18);
            this.lblCamera.TabIndex = 1;
            this.lblCamera.Text = "Camera";
            // 
            // lblCamin
            // 
            this.lblCamin.AutoSize = true;
            this.lblCamin.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamin.Location = new System.Drawing.Point(41, 104);
            this.lblCamin.Name = "lblCamin";
            this.lblCamin.Size = new System.Drawing.Size(56, 18);
            this.lblCamin.TabIndex = 2;
            this.lblCamin.Text = "Camin";
            // 
            // lblCNP
            // 
            this.lblCNP.AutoSize = true;
            this.lblCNP.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNP.Location = new System.Drawing.Point(41, 72);
            this.lblCNP.Name = "lblCNP";
            this.lblCNP.Size = new System.Drawing.Size(43, 18);
            this.lblCNP.TabIndex = 3;
            this.lblCNP.Text = "CNP";
            // 
            // txtNume
            // 
            this.txtNume.Location = new System.Drawing.Point(131, 41);
            this.txtNume.Name = "txtNume";
            this.txtNume.Size = new System.Drawing.Size(151, 20);
            this.txtNume.TabIndex = 4;
            // 
            // txtCNP
            // 
            this.txtCNP.Location = new System.Drawing.Point(131, 72);
            this.txtCNP.Name = "txtCNP";
            this.txtCNP.Size = new System.Drawing.Size(151, 20);
            this.txtCNP.TabIndex = 7;
            // 
            // cmbCamin
            // 
            this.cmbCamin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCamin.FormattingEnabled = true;
            this.cmbCamin.Items.AddRange(new object[] {
            "blalb"});
            this.cmbCamin.Location = new System.Drawing.Point(131, 108);
            this.cmbCamin.Name = "cmbCamin";
            this.cmbCamin.Size = new System.Drawing.Size(152, 21);
            this.cmbCamin.TabIndex = 8;
            this.cmbCamin.SelectedIndexChanged += new System.EventHandler(this.cmbCamin_SelectedIndexChanged_1);
            // 
            // cmbCamera
            // 
            this.cmbCamera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCamera.FormattingEnabled = true;
            this.cmbCamera.Location = new System.Drawing.Point(130, 135);
            this.cmbCamera.Name = "cmbCamera";
            this.cmbCamera.Size = new System.Drawing.Size(152, 21);
            this.cmbCamera.TabIndex = 9;
            // 
            // btnAdauga
            // 
            this.btnAdauga.Location = new System.Drawing.Point(44, 174);
            this.btnAdauga.Name = "btnAdauga";
            this.btnAdauga.Size = new System.Drawing.Size(101, 43);
            this.btnAdauga.TabIndex = 10;
            this.btnAdauga.Text = "Adauga";
            this.btnAdauga.UseVisualStyleBackColor = true;
            this.btnAdauga.Click += new System.EventHandler(this.btnAdauga_Click);
            // 
            // btnVizitatori
            // 
            this.btnVizitatori.Location = new System.Drawing.Point(176, 174);
            this.btnVizitatori.Name = "btnVizitatori";
            this.btnVizitatori.Size = new System.Drawing.Size(101, 45);
            this.btnVizitatori.TabIndex = 11;
            this.btnVizitatori.Text = " Vizitatori";
            this.btnVizitatori.UseVisualStyleBackColor = true;
            this.btnVizitatori.Click += new System.EventHandler(this.btnVizitatori_Click);
            // 
            // lstVizitatori
            // 
            this.lstVizitatori.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.id,
            this.Nume,
            this.CNP,
            this.OraSosire});
            this.lstVizitatori.Location = new System.Drawing.Point(310, 43);
            this.lstVizitatori.Name = "lstVizitatori";
            this.lstVizitatori.Size = new System.Drawing.Size(546, 176);
            this.lstVizitatori.TabIndex = 12;
            this.lstVizitatori.UseCompatibleStateImageBehavior = false;
            this.lstVizitatori.View = System.Windows.Forms.View.Details;
            this.lstVizitatori.SelectedIndexChanged += new System.EventHandler(this.lstVizitatori_SelectedIndexChanged);
            // 
            // id
            // 
            this.id.Text = "ID";
            this.id.Width = 27;
            // 
            // Nume
            // 
            this.Nume.Text = "Nume";
            this.Nume.Width = 185;
            // 
            // CNP
            // 
            this.CNP.Text = "CNP";
            this.CNP.Width = 211;
            // 
            // OraSosire
            // 
            this.OraSosire.Text = "OraSosire";
            // 
            // VizitatoriForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(882, 266);
            this.Controls.Add(this.lstVizitatori);
            this.Controls.Add(this.btnVizitatori);
            this.Controls.Add(this.btnAdauga);
            this.Controls.Add(this.cmbCamera);
            this.Controls.Add(this.cmbCamin);
            this.Controls.Add(this.txtCNP);
            this.Controls.Add(this.txtNume);
            this.Controls.Add(this.lblCNP);
            this.Controls.Add(this.lblCamin);
            this.Controls.Add(this.lblCamera);
            this.Controls.Add(this.lblNume);
            this.Name = "VizitatoriForm";
            this.Text = "VizitatoriForm";
            this.Load += new System.EventHandler(this.VizitatoriForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNume;
        private System.Windows.Forms.Label lblCamera;
        private System.Windows.Forms.Label lblCamin;
        private System.Windows.Forms.Label lblCNP;
        private System.Windows.Forms.TextBox txtNume;
        private System.Windows.Forms.TextBox txtCNP;
        private System.Windows.Forms.ComboBox cmbCamin;
        private System.Windows.Forms.ComboBox cmbCamera;
        private System.Windows.Forms.Button btnAdauga;
        private System.Windows.Forms.Button btnVizitatori;
        private System.Windows.Forms.ListView lstVizitatori;
        private System.Windows.Forms.ColumnHeader Nume;
        private System.Windows.Forms.ColumnHeader CNP;
        private System.Windows.Forms.ColumnHeader id;
        private System.Windows.Forms.ColumnHeader OraSosire;
    }
}