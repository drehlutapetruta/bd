﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;

namespace secondTry
{
    public partial class FormComplexCamin : Form
    {
        OdbcCommand comanda;
        OdbcConnection conexiune;
        DataSet dsDateFacultate, dsDateCamin, dsDateCamere;

        public FormComplexCamin()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {

                populeazaCamine();

            }

            catch (OdbcException eroare)
            {
                MessageBox.Show("A aparut eraoarea nr. " + eroare.ErrorCode.ToString() + " cu mesajul " + eroare.Message.ToString());
                if (conexiune.State == ConnectionState.Open)
                    conexiune.Close();

            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void populeazaCamine()
        {
            try
            {
                conexiune = new OdbcConnection();
                conexiune.ConnectionString = "Driver={PostgreSQL ANSI};database=CampusuriProiect;server=localhost;port=5432;uid=postgres;pwd=password;sslmode=disable;readonly=0;protocol=7.4;fakeoidindex=0;showoidcolumn=0;rowversioning=0;showsystemtables=0;fetch=100;unknownsizes=0;maxvarcharsize=255;maxlongvarcharsize=8190;debug=0;commlog=0;usedeclarefetch=0;textaslongvarchar=1;unknownsaslongvarchar=0;boolsaschar=1;parse=0;lfconversion=1;updatablecursors=1;trueisminus1=0;bi=0;byteaaslongvarbinary=1;useserversideprepare=1;lowercaseidentifier=0;xaopt=1;";
                conexiune.Open();
                comanda = new OdbcCommand();
                comanda.Connection = conexiune;
                comanda.CommandText = " Select * from camine";
                OdbcDataReader cititor;
                cititor = comanda.ExecuteReader();
                DataTable tblCamine;
                tblCamine = new DataTable("camine");
                tblCamine.Load(cititor);
                dsDateCamin = new DataSet();
                dsDateCamin.Tables.Add(tblCamine);
                comboBox1.DataSource = dsDateCamin.Tables["camine"];
                comboBox1.DisplayMember = "numecamin";
                comboBox1.ValueMember = "id_camin";
                comboBox1.Refresh();

            }
            catch (OdbcException eroare)
            {
                MessageBox.Show("Eroare:" + eroare.ErrorCode.ToString());

            }
}

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRowView nume = (DataRowView)comboBox1.SelectedItem;

            string numeCaminSelectat = nume["numecamin"].ToString();
            comanda = new OdbcCommand();
            comanda.CommandText = "select distinct numefac from facultate f inner join studenti s on s.id_facultate = f.id_facultate inner join infocamere ic on ic.id_camera = s.id_camera inner join camine c on c.id_camin = ic.id_camin where numecamin =?";
            comanda.Connection = conexiune;
            comanda.Parameters.Clear();
            comanda.Parameters.AddWithValue("numecamin", numeCaminSelectat);
            OdbcDataReader cititor;
            cititor = comanda.ExecuteReader();
            DataTable tblFacultate;
            tblFacultate = new DataTable("Facultate");
            tblFacultate.Load(cititor);
            dsDateFacultate = new DataSet();
            dsDateFacultate.Tables.Add(tblFacultate);

            //dataGridView2.DataMember = null;
            //dataGridView2.Refresh();


            dataGridView1.DataSource = dsDateFacultate;
            dataGridView1.DataMember = "Facultate";
            dataGridView1.Refresh();
            label2.Text = "Numarul de facultati din caminul selectat este: " + dsDateFacultate.Tables["Facultate"].Rows.Count.ToString() + ".";
            label3.Text = "Acestea sunt urmatoarele :";


        }
        private void dataGridView1_SelectionChanged (object sender, EventArgs e)
        {
            /* curat grid-ul cu numar camere
            dataGridView2.DataMember = null;
            dataGridView2.Refresh();

            DataRowView numeC = (DataRowView)comboBox1.SelectedItem;
            string numeCaminCurent = numeC["numecamin"].ToString();

            comanda = new OdbcCommand();
            comanda.CommandText = "select * from camere where  id_camin in (select id_camin from camine where numecamin=?)";
            comanda.Connection = conexiune;
            OdbcDataReader cititor;
            cititor = comanda.ExecuteReader();

            DataTable tblCamere;
            tblCamere = new DataTable("Camere");
            tblCamere.Load(cititor);

            dsDateCamere = new DataSet();
            dsDateCamere.Tables.Add(tblCamere);

            //stabilesc sursa de date pt gridul cu camere
            dataGridView2.DataSource = dsDateCamere;
            dataGridView2.DataMember = "camere";
            dataGridView2.Refresh();

            //pt actualizare cu nr camerelor
            comanda.CommandText = "select count(id_camera) from camere cam inner join camine c on cam.id_camin=c.id_camin where numecamin=?";
            comanda.Parameters.Clear();
            comanda.Parameters.AddWithValue("numecamin", numeCaminCurent);

            cititor = comanda.ExecuteReader();
           */ 

        }

    }
}
