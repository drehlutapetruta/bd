﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace secondTry
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }
        private void MainForm_FormClosed(object sender, FormClosedEventArgs e) => Application.Exit();

        private void studentiFormToolStripMenuItem_Click(object sender, EventArgs e)
        {
                
        }

        private void mnVizitatori_Click(object sender, EventArgs e)
        {
            VizitatoriForm Vf = new VizitatoriForm();
            Vf.Show();
        }

        private void adaugareStudentiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAdaugareStudenti ASf = new FormAdaugareStudenti();
            ASf.Show();
        }

        private void adaugareUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Adaugare_Useri Uf = new Adaugare_Useri();
            Uf.Show();
        }

        private void adaugareCaminToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormComplexInfoCamp Campf = new FormComplexInfoCamp();
            Campf.Show();
        }

        private void formComplexCaminToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormComplexCamin Caminf = new FormComplexCamin();
            Caminf.Show();
        }

        private void adaugareFacultateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAdaugareFac Facf = new FormAdaugareFac();
            Facf.Show();
        }
        
        private void formSimpluComplexCaminToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormSimpluComplexCamin FSCC = new FormSimpluComplexCamin();
            FSCC.Show();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void restantieriPlataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            restantieri_plata RPf = new restantieri_plata();
            RPf.Show();
        }

        private void adaugareChiriiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAdaugaChirie ChirieF = new FormAdaugaChirie();
            ChirieF.Show();
        }

        private void updateStudentiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormUpdateStud UpdateF = new FormUpdateStud();
            UpdateF.Show();
        }
    }
}
