﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;


namespace secondTry
{
    public partial class VizitatoriForm : Form
    {
        public VizitatoriForm()
        {
            InitializeComponent();
            CaminPopulare();
            cmbCamin.SelectedIndexChanged += new EventHandler(cmbCamin_SelectedIndexChanged);
            cmbCamin.SelectedIndex = -1;
        }

        public void CaminPopulare()
        {
            string connstring = "Driver={PostgreSQL ANSI}; database = Practica; server = localhost; port = 5432; uid = postgres; sslmode = disable; readonly= 0; protocol = 7.4; fakeoidindex = 0; showoidcolumn = 0; rowversioning = 0; showsystemtables = 0; fetch = 100; socket = 4096; unknownsizes = 0; maxvarcharsize = 255; maxlongvarcharsize = 8190; debug = 0; commlog = 0; optimizer = 0; ksqo = 1; usedeclarefetch = 0; textaslongvarchar = 1; unknownsaslongvarchar = 0; boolsaschar = 1; parse = 0; cancelasfreestmt = 0; extrasystableprefixes = dd_; lfconversion = 1; updatablecursors = 1; disallowpremature = 0; trueisminus1 = 0; bi = 0; byteaaslongvarbinary = 0; useserversideprepare = 0; lowercaseidentifier = 0; gssauthusegss = 0; xaopt = 1; pwd = password;";

            OdbcConnection connection = new OdbcConnection(connstring);
            connection.Open();

            //Preluam date din BD
            OdbcCommand comanda;
            comanda = new OdbcCommand();
            comanda.Connection = connection;
            string query = "SELECT id_camin, NumeCamin FROM Camine";
            comanda.CommandText = query;

            //Creare reader
            OdbcDataReader cititor;
            cititor = comanda.ExecuteReader();

            //Creare tabela locala
            DataTable tblCamine;
            tblCamine = new DataTable("CamineLocal");
            tblCamine.Load(cititor);

            DataSet camine = new DataSet();
            camine.Tables.Add(tblCamine);

            cmbCamin.DataSource = camine.Tables[0];
            cmbCamin.ValueMember = "id_camin";
            cmbCamin.DisplayMember = "NumeCamin";
        }

        private void cmbCamin_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCamin.SelectedValue != null)
            {
                string connstring = "Driver={PostgreSQL ANSI}; database = Practica; server = localhost; port = 5432; uid = postgres; sslmode = disable; readonly= 0; protocol = 7.4; fakeoidindex = 0; showoidcolumn = 0; rowversioning = 0; showsystemtables = 0; fetch = 100; socket = 4096; unknownsizes = 0; maxvarcharsize = 255; maxlongvarcharsize = 8190; debug = 0; commlog = 0; optimizer = 0; ksqo = 1; usedeclarefetch = 0; textaslongvarchar = 1; unknownsaslongvarchar = 0; boolsaschar = 1; parse = 0; cancelasfreestmt = 0; extrasystableprefixes = dd_; lfconversion = 1; updatablecursors = 1; disallowpremature = 0; trueisminus1 = 0; bi = 0; byteaaslongvarbinary = 0; useserversideprepare = 0; lowercaseidentifier = 0; gssauthusegss = 0; xaopt = 1; pwd = password;";

                OdbcConnection connection = new OdbcConnection(connstring);
                connection.Open();

                //Preluam date din BD
                OdbcCommand F_Camera;
                F_Camera = new OdbcCommand("f_camera", connection);
                F_Camera.CommandType = CommandType.StoredProcedure;
                F_Camera.Parameters.Add("id_camin_", NpgsqlTypes.NpgsqlDbType.Smallint);
                F_Camera.Parameters["id_camin_"].Value = cmbCamin.SelectedValue;

                //Creare reader
                OdbcDataReader cititor;
                cititor = F_Camera.ExecuteReader();

                cmbCamera.Items.Clear();
                while (cititor.Read())

                {
                    cmbCamera.Items.Add(cititor[0]);

                }

                connection.Close();
            }
        }

        public static String GetDate(DateTime value)
        {
            return value.ToString("dd.MM.yyyy");
        }
        public static String GetTimeStamp(DateTime value)
        {
            return value.ToString("HH:mm:ss");
        }
        private void btnAdauga_Click(object sender, EventArgs e)
        {
            string CNP = txtCNP.Text;
            string Nume = txtNume.Text;

            DataRowView oDataRowView = cmbCamin.SelectedItem as DataRowView;
            string Camin = string.Empty;

            if (oDataRowView != null)
            {
                Camin = oDataRowView.Row["NumeCamin"] as string;
            }

            int nrCamera = 0;
            Int32.TryParse(cmbCamera.Text, out nrCamera);
            string dataIntrare = GetDate(DateTime.Now);
            string OraSosire = GetTimeStamp(DateTime.Now);
            string OraPlecare = GetTimeStamp(DateTime.Now);
            string query = "INSERT INTO Vizitatori(Nume_Vizitator, CNP_Vizitator, NumeCamin, NrCamera, DataVizitei, OraSosire, OraPlecare)"
                            + "Values('" + Nume + "', '" + CNP + "', '" + Camin + "', '" + nrCamera + "' , '" + dataIntrare + "' , '" + OraSosire + "' , '" + OraPlecare + "')";

            string connstring = "Server = 127.0.0.1; Port = 5432; User Id = postgres; Password = password; Database = Practica;";
            //"Driver={PostgreSQL ANSI}; database = CampusuriProiect; server = localhost; port = 5432; uid = postgres; sslmode = disable; readonly= 0; protocol = 7.4; fakeoidindex = 0; showoidcolumn = 0; rowversioning = 0; showsystemtables = 0; fetch = 100; socket = 4096; unknownsizes = 0; maxvarcharsize = 255; maxlongvarcharsize = 8190; debug = 0; commlog = 0; optimizer = 0; ksqo = 1; usedeclarefetch = 0; textaslongvarchar = 1; unknownsaslongvarchar = 0; boolsaschar = 1; parse = 0; cancelasfreestmt = 0; extrasystableprefixes = dd_; lfconversion = 1; updatablecursors = 1; disallowpremature = 0; trueisminus1 = 0; bi = 0; byteaaslongvarbinary = 0; useserversideprepare = 0; lowercaseidentifier = 0; gssauthusegss = 0; xaopt = 1; pwd = password;";

            OdbcConnection connection = new OdbcConnection(connstring);

            connection.Open();
            OdbcCommand Adauga;
            Adauga = new OdbcCommand(query, connection);
            Adauga.ExecuteNonQuery();

        }

        private void btnVizitatori_Click(object sender, EventArgs e)
        {

            lstVizitatori.Items.Clear();
            string connstring = "Driver={PostgreSQL ANSI}; database = Practica; server = localhost; port = 5432; uid = postgres; sslmode = disable; readonly= 0; protocol = 7.4; fakeoidindex = 0; showoidcolumn = 0; rowversioning = 0; showsystemtables = 0; fetch = 100; socket = 4096; unknownsizes = 0; maxvarcharsize = 255; maxlongvarcharsize = 8190; debug = 0; commlog = 0; optimizer = 0; ksqo = 1; usedeclarefetch = 0; textaslongvarchar = 1; unknownsaslongvarchar = 0; boolsaschar = 1; parse = 0; cancelasfreestmt = 0; extrasystableprefixes = dd_; lfconversion = 1; updatablecursors = 1; disallowpremature = 0; trueisminus1 = 0; bi = 0; byteaaslongvarbinary = 0; useserversideprepare = 0; lowercaseidentifier = 0; gssauthusegss = 0; xaopt = 1; pwd = password;";

            OdbcConnection connection = new OdbcConnection(connstring);
            connection.Open();

            //Preluam date din BD
            OdbcCommand Afiseaza;
            Afiseaza = new OdbcCommand();
            Afiseaza.Connection = connection;
            string query = "SELECT id_vizitator, Nume_Vizitator, CNP_Vizitator, OraSosire FROM Vizitatori";
            Afiseaza.CommandText = query;
            OdbcDataReader cititor;
            cititor = Afiseaza.ExecuteReader();
            DataTable tblVizitatori;
            tblVizitatori = new DataTable("VizitatoriLocal");
            tblVizitatori.Load(cititor);

            DataSet camine = new DataSet();
            camine.Tables.Add(tblVizitatori);

            for (int i = 0; i < tblVizitatori.Rows.Count; i++)
            {
                DataRow dr = tblVizitatori.Rows[i];
                ListViewItem listitem = new ListViewItem(dr["id_vizitator"].ToString());
                listitem.SubItems.Add(dr["Nume_Vizitator"].ToString());
                listitem.SubItems.Add(dr["CNP_Vizitator"].ToString());
                listitem.SubItems.Add(dr["OraSosire"].ToString());
                lstVizitatori.Items.Add(listitem);
            }

        }

        private void lstVizitatori_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbCamin_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void VizitatoriForm_Load(object sender, EventArgs e)
        {

        }
    }
}
