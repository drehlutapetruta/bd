﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;

namespace secondTry
{
    public partial class FormComplexInfoCamp : Form
    {
        OdbcCommand comanda;
        OdbcConnection conexiune;
        DataSet dsDate;
        int indexInregistrareCurenta;

        public FormComplexInfoCamp()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnModificare_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox1.Focus();
            btnSalvare.Visible = true;
            btnAnulare.Visible = true;

            textBox1.Visible = true;
            btnModificare.Visible = false;

        }

        private void btnSalvare_Click(object sender, EventArgs e)
        {
            try
            {
                //inserez in baza de date inregistrarea curenta
                OdbcCommand comanda = new OdbcCommand();
                comanda.CommandText = "INSERT INTO campusuri VALUES(?,?,?,?)";
                comanda.Connection = conexiune;
                //stabilim valorile parametrilor in functie de ce a introdus utilizatorul
                comanda.Parameters.AddWithValue("", textBox1.Text.ToString());
                comanda.Parameters.AddWithValue("", textBox2.Text.ToString());
                comanda.Parameters.AddWithValue("", textBox3.Text.ToString());
                comanda.Parameters.AddWithValue("", textBox4.Text.ToString());
                comanda.ExecuteNonQuery();

                MessageBox.Show("Campusul a fost adaugat in baza de date!");
                btnSalvare.Visible = false;
                btnAnulare.Visible = false;
                textBox1.Visible = true;
                btnModificare.Visible = true;
                DeschideConexiunea();
                afisareInregistrareCurenta(indexInregistrareCurenta);
            }
            catch (OdbcException eroare)
            {
                MessageBox.Show("A aparut eroarea nr. " + eroare.ErrorCode.ToString() + " cu mesajul " +
                eroare.Message.ToString());
            }

        }

        private void DeschideConexiunea()
        {
            try
            {

                conexiune = new OdbcConnection();

                conexiune.ConnectionString = "Driver={PostgreSQL ANSI}; Dsn=PostgreSQL30;database=CampusuriProiect;server=localhost;port=5432;uid=postgres;sslmode=disable;readonly=0;protocol=7.4;fakeoidindex=0;showoidcolumn=0;rowversioning=0;showsystemtables=0;fetch=100;unknownsizes=0;maxvarcharsize=255;maxlongvarcharsize=8190;debug=0;commlog=0;usedeclarefetch=0;textaslongvarchar=1;unknownsaslongvarchar=0;boolsaschar=1;parse=0;extrasystableprefixes=dd_;lfconversion=1;updatablecursors=1;trueisminus1=0;bi=0;byteaaslongvarbinary=0;useserversideprepare=1;lowercaseidentifier=0;gssauthusegss=0;xaopt=1;password=password";
                conexiune.Open();
                OdbcCommand comanda;
                comanda = new OdbcCommand();
                comanda.Connection = conexiune;
                comanda.CommandText = "SELECT * FROM campusuri";

                OdbcDataReader cititor;
                cititor = comanda.ExecuteReader();

                DataTable tblCampusuri;

                tblCampusuri = new DataTable("CAMPUSURI");
                tblCampusuri.Load(cititor);


                dsDate = new DataSet();
                dsDate.Tables.Add(tblCampusuri);

                //nrTotalInregistrari = dsDate.Tables["Camine"].Rows.Count;

                this.indexInregistrareCurenta = 0;

                dataGridView1.DataSource = dsDate;
                dataGridView1.DataMember = "Campusuri";
                dataGridView1.Refresh();
            }
            catch (OdbcException eroare)
            {
                MessageBox.Show("A aparut eroarea nr. " + eroare.ErrorCode.ToString() + " cu mesajul " +
                eroare.Message.ToString());
                if (conexiune.State == ConnectionState.Open)
                    conexiune.Close();
            }
        }

        private void btnAnulare_Click(object sender, EventArgs e)
        {
            btnAnulare.Visible = false;
            btnSalvare.Visible = false;

            btnModificare.Visible = true;
            afisareInregistrareCurenta(indexInregistrareCurenta);
            textBox1.Visible = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.DeschideConexiunea();
            this.afisareInregistrareCurenta(indexInregistrareCurenta);
            //populeaza();
            btnAnulare.Visible = false;
            btnSalvare.Visible = false;


        }
        /*private void populeaza()
        {
            OdbcCommand comanda;
            comanda = new OdbcCommand();
            comanda.Connection = conexiune;
            comanda.CommandText = "SELECT distinct id_campus, numecampus, cantina, nrcamere FROM campusuri ORDER BY id_campus";

            OdbcDataReader cititor;
            cititor = comanda.ExecuteReader();

            DataTable tblCampusuri;
            tblCampusuri = new DataTable("Campusuri");
            tblCampusuri.Load(cititor);

            dsDate.Tables.Add(tblCampusuri);
            //incerc sa populez combobox-ul
            comboBox1.DataSource = dsDate.Tables["Campusuri"];
            comboBox1.DisplayMember = "Id_Campus";
            comboBox1.ValueMember = "id_campus";
        }
        */

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
        private void afisareInregistrareCurenta(int nrInregistrare)
        {
            textBox1.Text = dsDate.Tables["Campusuri"].Rows[nrInregistrare].ItemArray[0].ToString();        
            textBox2.Text = dsDate.Tables["Campusuri"].Rows[nrInregistrare].ItemArray[1].ToString();
            textBox3.Text = dsDate.Tables["Campusuri"].Rows[nrInregistrare].ItemArray[2].ToString();
            textBox4.Text = dsDate.Tables["Campusuri"].Rows[nrInregistrare].ItemArray[3].ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (indexInregistrareCurenta == 0)
                indexInregistrareCurenta = dsDate.Tables["Campusuri"].Rows.Count - 1;

            else
                indexInregistrareCurenta--;

            afisareInregistrareCurenta(indexInregistrareCurenta);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (indexInregistrareCurenta == dsDate.Tables["Campusuri"].Rows.Count - 1)
                indexInregistrareCurenta = 0;
            else
                indexInregistrareCurenta++;

            afisareInregistrareCurenta(indexInregistrareCurenta);

        }
    }
}

