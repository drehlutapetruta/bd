﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;

namespace secondTry
{
    public partial class FormAdaugareFac : Form
    {
        OdbcConnection conexiune;
        OdbcCommand comanda;
        DataSet dsDate;
        DataTable tblFacultate;
        OdbcDataReader cititor;
        int inregistrareCurenta;


        public FormAdaugareFac()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            conexiune = new OdbcConnection();
            conexiune.ConnectionString= "Driver={PostgreSQL ANSI}; Dsn=PostgreSQL30;database=CampusuriProiect;server=localhost;port=5432;uid=postgres;sslmode=disable;readonly=0;protocol=7.4;fakeoidindex=0;showoidcolumn=0;rowversioning=0;showsystemtables=0;fetch=100;unknownsizes=0;maxvarcharsize=255;maxlongvarcharsize=8190;debug=0;commlog=0;usedeclarefetch=0;textaslongvarchar=1;unknownsaslongvarchar=0;boolsaschar=1;parse=0;extrasystableprefixes=dd_;lfconversion=1;updatablecursors=1;trueisminus1=0;bi=0;byteaaslongvarbinary=0;useserversideprepare=1;lowercaseidentifier=0;gssauthusegss=0;xaopt=1;password=password";
            conexiune.Open();
            comanda = new OdbcCommand();
            comanda.Connection = conexiune;
            comanda.CommandText = "SELECT * FROM facultate";
            cititor = comanda.ExecuteReader();


            tblFacultate = new DataTable("facultate");
            tblFacultate.Load(cititor);


            dsDate = new DataSet();
            dsDate.Tables.Add(tblFacultate);
            afiseazaInregistrareCurenta(inregistrareCurenta);

        }
        private void afiseazaInregistrareCurenta(int i)
        {
            textBox1.Text = dsDate.Tables["Facultate"].Rows[i].ItemArray[0].ToString();
            textBox2.Text = dsDate.Tables["Facultate"].Rows[i].ItemArray[1].ToString();
            textBox3.Text = dsDate.Tables["Facultate"].Rows[i].ItemArray[2].ToString();
        }

        private void btnUrmator_Click(object sender, EventArgs e)
        {
            if (inregistrareCurenta >= dsDate.Tables["facultate"].Rows.Count - 1)
                inregistrareCurenta = 0;
            else
                inregistrareCurenta++;
            afiseazaInregistrareCurenta(inregistrareCurenta);
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            if (inregistrareCurenta == 0)
                inregistrareCurenta = dsDate.Tables["facultate"].Rows.Count - 1;
            else
                inregistrareCurenta--;
            afiseazaInregistrareCurenta(inregistrareCurenta);
        }

        private void btnUltim_Click(object sender, EventArgs e)
        {
            inregistrareCurenta = dsDate.Tables["facultate"].Rows.Count - 1;
            afiseazaInregistrareCurenta(inregistrareCurenta);
        }

        private void btnPrimul_Click(object sender, EventArgs e)
        {
            inregistrareCurenta = 0;
            afiseazaInregistrareCurenta(inregistrareCurenta);
        }

        private void btnAdaugare_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            btnSalvare.Visible = true;
            btnAdaugare.Visible = false;
            btnUrmator.Visible = false;
            btnAnterior.Visible = false;
            btnPrimul.Visible = false;
            btnUltim.Visible = false;
            btnAnulare.Visible = true;
            btnStergere.Visible = false;
            textBox1.Focus();

        }

        private void btnSalvare_Click(object sender, EventArgs e)
        {
            try
            {
                comanda.CommandText = "insert into facultate values(?,?,?) ";
                comanda.Parameters.Clear();
                comanda.Parameters.AddWithValue("id_facultate", textBox1.Text);
                comanda.Parameters.AddWithValue("numefac", textBox2.Text);
                comanda.Parameters.AddWithValue("numedecan", textBox3.Text);
                comanda.ExecuteNonQuery();
                MessageBox.Show("Ai reusit sa adaugi o facultate! Felicitari! :)");
                btnSalvare.Visible = false;
                btnAdaugare.Visible = true;
                btnUrmator.Visible = true;
                btnAnterior.Visible = true;
                btnPrimul.Visible = true;
                btnUltim.Visible = true;
                btnAnulare.Visible =false;
                btnStergere.Visible = true;

            }
            catch(Exception eroare)
            {
                MessageBox.Show("Eroarea: " + eroare.Message);
            }

        }

        private void btnAnulare_Click(object sender, EventArgs e)
        {
            btnSalvare.Visible = false;
            btnAdaugare.Visible = true;
            btnUrmator.Visible = true;
            btnAnterior.Visible = true;
            btnPrimul.Visible = true;
            btnUltim.Visible = true;
            btnAnulare.Visible = false;
            btnStergere.Visible = true;

            afiseazaInregistrareCurenta(inregistrareCurenta);
        }

        private void btnStergere_Click(object sender, EventArgs e)
        {
            comanda.CommandText = "delete from facultate where id_facultate =?";
            comanda.Parameters.Clear();
            comanda.Parameters.AddWithValue("id_facultate", textBox1.Text);
            int nrInregAfect = comanda.ExecuteNonQuery();
            if (nrInregAfect == 0)
                MessageBox.Show("Atentie! Nu ai sters nimic.");
            else
            {
                MessageBox.Show("Felicitari! Ai sters o facultate!");
                inregistrareCurenta++;
                afiseazaInregistrareCurenta(inregistrareCurenta);
            }
        }
    }
}
