﻿namespace secondTry
{
    partial class FormAdaugaChirie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.txtid_stud = new System.Windows.Forms.TextBox();
            this.txtid_camin = new System.Windows.Forms.TextBox();
            this.txtNumeStudent = new System.Windows.Forms.TextBox();
            this.txtPrenumeStudent = new System.Windows.Forms.TextBox();
            this.txtChirie = new System.Windows.Forms.TextBox();
            this.txtSoldCurent = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(289, 335);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(138, 52);
            this.button1.TabIndex = 0;
            this.button1.Text = "Adaugare student";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtid_stud
            // 
            this.txtid_stud.Location = new System.Drawing.Point(299, 12);
            this.txtid_stud.Name = "txtid_stud";
            this.txtid_stud.Size = new System.Drawing.Size(128, 20);
            this.txtid_stud.TabIndex = 1;
            // 
            // txtid_camin
            // 
            this.txtid_camin.Location = new System.Drawing.Point(299, 52);
            this.txtid_camin.Name = "txtid_camin";
            this.txtid_camin.Size = new System.Drawing.Size(128, 20);
            this.txtid_camin.TabIndex = 2;
            this.txtid_camin.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // txtNumeStudent
            // 
            this.txtNumeStudent.Location = new System.Drawing.Point(299, 99);
            this.txtNumeStudent.Name = "txtNumeStudent";
            this.txtNumeStudent.Size = new System.Drawing.Size(128, 20);
            this.txtNumeStudent.TabIndex = 3;
            // 
            // txtPrenumeStudent
            // 
            this.txtPrenumeStudent.Location = new System.Drawing.Point(299, 146);
            this.txtPrenumeStudent.Name = "txtPrenumeStudent";
            this.txtPrenumeStudent.Size = new System.Drawing.Size(128, 20);
            this.txtPrenumeStudent.TabIndex = 4;
            this.txtPrenumeStudent.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // txtChirie
            // 
            this.txtChirie.Location = new System.Drawing.Point(299, 190);
            this.txtChirie.Name = "txtChirie";
            this.txtChirie.Size = new System.Drawing.Size(128, 20);
            this.txtChirie.TabIndex = 5;
            // 
            // txtSoldCurent
            // 
            this.txtSoldCurent.Location = new System.Drawing.Point(299, 245);
            this.txtSoldCurent.Name = "txtSoldCurent";
            this.txtSoldCurent.Size = new System.Drawing.Size(128, 20);
            this.txtSoldCurent.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(193, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Id student";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(193, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Id camin";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(193, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Nume student";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(193, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Prenume Student";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(193, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Chirie";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(193, 245);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Sold curent";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSoldCurent);
            this.Controls.Add(this.txtChirie);
            this.Controls.Add(this.txtPrenumeStudent);
            this.Controls.Add(this.txtNumeStudent);
            this.Controls.Add(this.txtid_camin);
            this.Controls.Add(this.txtid_stud);
            this.Controls.Add(this.button1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtid_stud;
        private System.Windows.Forms.TextBox txtid_camin;
        private System.Windows.Forms.TextBox txtNumeStudent;
        private System.Windows.Forms.TextBox txtPrenumeStudent;
        private System.Windows.Forms.TextBox txtChirie;
        private System.Windows.Forms.TextBox txtSoldCurent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}