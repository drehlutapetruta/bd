﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;

namespace secondTry
{
    public partial class Adaugare_Useri : Form
    {
        public Adaugare_Useri()
        {
            InitializeComponent();
            AdminiPopulare();
        }

        public void AdminiPopulare()
        {
            string connstring = "Server = 127.0.0.1; Port = 5432; User Id = postgres; Password = password; Database = Practica;";
            //"Driver={PostgreSQL ANSI}; database = CampusuriProiect; server = localhost; port = 5432; uid = postgres; sslmode = disable; readonly= 0; protocol = 7.4; fakeoidindex = 0; showoidcolumn = 0; rowversioning = 0; showsystemtables = 0; fetch = 100; socket = 4096; unknownsizes = 0; maxvarcharsize = 255; maxlongvarcharsize = 8190; debug = 0; commlog = 0; optimizer = 0; ksqo = 1; usedeclarefetch = 0; textaslongvarchar = 1; unknownsaslongvarchar = 0; boolsaschar = 1; parse = 0; cancelasfreestmt = 0; extrasystableprefixes = dd_; lfconversion = 1; updatablecursors = 1; disallowpremature = 0; trueisminus1 = 0; bi = 0; byteaaslongvarbinary = 0; useserversideprepare = 0; lowercaseidentifier = 0; gssauthusegss = 0; xaopt = 1; pwd = password;";

            OdbcConnection connection = new OdbcConnection(connstring);
            connection.Open();

            //Preluam date din BD
            OdbcCommand comanda;
            comanda = new OdbcCommand();
            comanda.Connection = connection;
            string query = "SELECT id_admin, Username FROM Admini";
            comanda.CommandText = query;

            //Creare reader
            OdbcDataReader cititor;
            cititor = comanda.ExecuteReader();

            //Creare tabela locala
            DataTable tblAdmini;
            tblAdmini = new DataTable("AdminiLocal");
            tblAdmini.Load(cititor);

            DataSet admini = new DataSet();
            admini.Tables.Add(tblAdmini);

            cmbUseri.DataSource = admini.Tables[0];
            cmbUseri.ValueMember = "id_admin";
            cmbUseri.DisplayMember = "Username";
        }

        

        private void btnAdauga_Click(object sender, EventArgs e)
        {
            string nume = txtNume.Text;
            string email = txtMail.Text;
            string user = txtUsername.Text;
            string parola = txtParola.Text;
            string grad = "";
            if (radioSef.Checked)
            {
                grad = "1";
            }
            else if (radioSecretara.Checked)
            {
                grad = "2";
            }
            else
            {
                grad = "3";
            }
            
            string query = "INSERT INTO Admini (Nume_admin, Email, Username, Parola, Grad)"
                         + "Values('" + nume + "', '" + email + "', '" + user + "', '" + parola + "' , '" + grad + "')";

            string connstring = "Server = 127.0.0.1; Port = 5432; User Id = postgres; Password = password; Database = Practica;";
            //"Driver={PostgreSQL ANSI}; database = CampusuriProiect; server = localhost; port = 5432; uid = postgres; sslmode = disable; readonly= 0; protocol = 7.4; fakeoidindex = 0; showoidcolumn = 0; rowversioning = 0; showsystemtables = 0; fetch = 100; socket = 4096; unknownsizes = 0; maxvarcharsize = 255; maxlongvarcharsize = 8190; debug = 0; commlog = 0; optimizer = 0; ksqo = 1; usedeclarefetch = 0; textaslongvarchar = 1; unknownsaslongvarchar = 0; boolsaschar = 1; parse = 0; cancelasfreestmt = 0; extrasystableprefixes = dd_; lfconversion = 1; updatablecursors = 1; disallowpremature = 0; trueisminus1 = 0; bi = 0; byteaaslongvarbinary = 0; useserversideprepare = 0; lowercaseidentifier = 0; gssauthusegss = 0; xaopt = 1; pwd = password;";

            OdbcConnection connection = new OdbcConnection(connstring);

            connection.Open();
            OdbcCommand Adauga;
            Adauga = new OdbcCommand(query, connection);
            Adauga.ExecuteNonQuery();

        }

        private void btnSterge_Click(object sender, EventArgs e)
        {
            DataRowView oDataRowView = cmbUseri.SelectedItem as DataRowView;
            string User = string.Empty;

            if (oDataRowView != null)
            {
                User = oDataRowView.Row["Username"] as string;
            }
            string query = "DELETE FROM Admini WHERE Username = '" + User + "'";

            string connstring = "Server = 127.0.0.1; Port = 5432; User Id = postgres; Password = password; Database = Practica;";
            //"Driver={PostgreSQL ANSI}; database = CampusuriProiect; server = localhost; port = 5432; uid = postgres; sslmode = disable; readonly= 0; protocol = 7.4; fakeoidindex = 0; showoidcolumn = 0; rowversioning = 0; showsystemtables = 0; fetch = 100; socket = 4096; unknownsizes = 0; maxvarcharsize = 255; maxlongvarcharsize = 8190; debug = 0; commlog = 0; optimizer = 0; ksqo = 1; usedeclarefetch = 0; textaslongvarchar = 1; unknownsaslongvarchar = 0; boolsaschar = 1; parse = 0; cancelasfreestmt = 0; extrasystableprefixes = dd_; lfconversion = 1; updatablecursors = 1; disallowpremature = 0; trueisminus1 = 0; bi = 0; byteaaslongvarbinary = 0; useserversideprepare = 0; lowercaseidentifier = 0; gssauthusegss = 0; xaopt = 1; pwd = password;";

            OdbcConnection connection = new OdbcConnection(connstring);

            connection.Open();
            OdbcCommand Adauga;
            Adauga = new OdbcCommand(query, connection);
            Adauga.ExecuteNonQuery();
            
            cmbUseri.Refresh();

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            
            cmbUseri.DisplayMember = null;
            cmbUseri.Refresh();
        }

        private void Adaugare_Useri_Load(object sender, EventArgs e)
        {

        }
    }
}
