﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;
using Npgsql;
using System.Security.Cryptography;


namespace secondTry
{
    public partial class LoginForm : Form
    {

        public LoginForm()
        {
            InitializeComponent();
        }

        
        private void Form1_Load(object sender, EventArgs e)
        {

        }
       

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string connstring = "Server = 127.0.0.1; Port = 5432; User Id = postgres; Password = password; Database = Practica;";
            //"Driver={PostgreSQL ANSI}; database = CampusuriProiect; server = localhost; port = 5432; uid = postgres; sslmode = disable; readonly= 0; protocol = 7.4; fakeoidindex = 0; showoidcolumn = 0; rowversioning = 0; showsystemtables = 0; fetch = 100; socket = 4096; unknownsizes = 0; maxvarcharsize = 255; maxlongvarcharsize = 8190; debug = 0; commlog = 0; optimizer = 0; ksqo = 1; usedeclarefetch = 0; textaslongvarchar = 1; unknownsaslongvarchar = 0; boolsaschar = 1; parse = 0; cancelasfreestmt = 0; extrasystableprefixes = dd_; lfconversion = 1; updatablecursors = 1; disallowpremature = 0; trueisminus1 = 0; bi = 0; byteaaslongvarbinary = 0; useserversideprepare = 0; lowercaseidentifier = 0; gssauthusegss = 0; xaopt = 1; pwd = password;";

            NpgsqlConnection connection = new NpgsqlConnection(connstring);
            connection.Open();

            //Preluam date din BD
            NpgsqlCommand comanda;
            comanda = new NpgsqlCommand();
            comanda.Connection = connection;
            string query = "SELECT * FROM Admini WHERE Username='" + txtUsername.Text + "'";
            comanda.CommandText = query;

            //Creare reader
            NpgsqlDataReader cititor;
            cititor = comanda.ExecuteReader();

            //Creare tabela locala
            DataTable tblAdmini;
            tblAdmini = new DataTable("Admini");
            tblAdmini.Load(cititor);

            //Verificam credidentialele
            using (MD5 md5Hash = MD5.Create())
            {
                string hash = GetMd5Hash(md5Hash, txtPassword.Text);
                bool exists = tblAdmini.Select().ToList().Exists(row => row["Username"].ToString() == txtUsername.Text);
                if (exists)
                {
                    if (tblAdmini.Rows[0][4].ToString().Trim() == hash)
                    {
                        MainForm Mf = new MainForm();
                        Mf.Show();
                        this.Hide();
                    }
                    else MessageBox.Show("Username sau parola gresita!");
                } else MessageBox.Show("Username sau parola gresita!");
            }
        }


        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
        
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
